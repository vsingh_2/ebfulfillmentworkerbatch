#!/bin/bash
callspringbatch()
{
while true
do 
echo "Batch job start"
BATCH_EXEC="java -cp ./spring-batch-core-2.1.8.RELEASE.jar:./target/WorkerBatch.jar:./target/original-WorkerBatch.jar org.springframework.batch.core.launch.support.CommandLineJobRunner loadfulfillmentRuleJob-context.xml loadfulfillmentRule"
echo $BATCH_EXEC
eval $BATCH_EXEC
exitIfError $? "Error running batch program." 100
echo "Batch job before sleep"
sleep 300
echo "Batch job after sleep"
done
}

exitIfError()
{
  if [ $1 -ne 0 ]
    then
      echo "$2 Code: $3"
	  echo "Before email sending......"
	  topicARN=arn:aws:sns:us-east-1:691339910992:hssrd-testing
	  subj="Event Broker Admin Fulfillment Rule Batch Job Failed."
	  aws configure set default.region us-east-1
	  touch message.txt
	  curl http://169.254.169.254/latest/meta-data/instance-id > message.txt
	  msg=$(cat message.txt)
	  aws sns publish --topic-arn $topicARN --subject "${subj}" --message "${msg}"
	  echo "After email sending......"
      exit $3
  else
	echo "Batch job successfully executed."
  fi
}


callspringbatch

