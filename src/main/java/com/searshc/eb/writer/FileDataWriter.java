package com.searshc.eb.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.searshc.eb.dao.FulfillmentRuleDAO;
import com.searshc.eb.fulfillment.domain.FileData;

public class FileDataWriter implements ItemWriter<FileData> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileDataWriter.class);

	@Autowired
	FulfillmentRuleDAO ruleDao;

	@Override
	public void write(List<? extends FileData> fileDataList) throws Exception {
		for (FileData data : fileDataList) {
			if (!data.checkEmpty()) {
				LOGGER.info("Storing rule {} to database ", data);
				ruleDao.save(data);
			}
		}
	}
}
