package com.searshc.eb.constant;

public final class Constants {

	public static final String USER_KEY = "";
	public static final String USER_SECRET = "";
	//public static final String SENDER_EMAIL = "HS_SRD_TEAM@searshc.com";
	public static final String TEMPLATE_NAME = "FulfilementBatchTemplate";
	// for testing purpose both are same
	//public static final String RECEVIER_EMAIL = "HS_SRD_TEAM@searshc.com";
	public static final String RECEVIER_EMAIL = "vinod.patidar@searshc.com";
	public static final String SENDER_EMAIL = "vinod.patidar@searshc.com";
	public static final String EBADMIN_USER = "EBADMIN";
	public static final String UNIT = "UNIT";
	public static final String USER = "User";
	public static final String SPEC = "SPEC";
	public static final String MERCH = "MERCH";
	public static final String ZIP = "ZIP";
	public static final String IRU = "IRU";
	public static final String UNITS = "Unit";
	public static final String FRU = "FRU";
	public static final String SEPERATOR = "\\|";
	public static final String BUSINESS_FLOW = "FUL";
	public static final Character USER_TYPE = 'B';
	public static final String LOCK_USER = "Batch";
	
	//Cache name for all lookup data
	public static final String BRANDLOOKUP_CACHE = "BrandLookup";
	public static final String CHANNELLOOKUP_CACHE = "ChannelLookup";
	public static final String CLIENTLOOKUP_CACHE = "ClientLookup";
	public static final String COVERAGELOOKUP_CACHE = "CoverageLookup";
	public static final String CREATORTYPELOOKUP_CACHE = "CreatorTypeLookup";
	public static final String GEOGRAPHYTYPELOOKUP_CACHE = "GeographyTypeLookup";
	public static final String HELPERTYPELOOKUP_CACHE = "HelperTypeLookup";
	public static final String MERCHANDISETYPELOOKUP_CACHE = "MerchandiseTypeLookup";
	public static final String PARTSTATUSTYPELOOKUP_CACHE = "PartStatusTypeLookup";
	public static final String PROCESSIDLOOKUP_CACHE = "ProcessIdLookup";
	public static final String SERVICEFLAGLOOKUP_CACHE = "ServiceFlagLookup";
	public static final String SERVICEPROVIDERLOOKUP_CACHE = "ServiceProviderLookup";
	public static final String SERVICETYPELOOKUP_CACHE = "ServiceTypeLookup";
}
