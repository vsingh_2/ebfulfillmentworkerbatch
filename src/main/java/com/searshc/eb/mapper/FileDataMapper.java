package com.searshc.eb.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/*import org.springframework.batch.item.excel.RowMapper;
import org.springframework.batch.item.excel.support.rowset.RowSet;*/



import com.searshc.eb.fulfillment.domain.FileData;
import com.searshc.hs.batch.item.excel.RowMapper;
import com.searshc.hs.batch.item.excel.support.rowset.RowSet;

public class FileDataMapper implements RowMapper<FileData> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileDataMapper.class);

	@Override
	public FileData mapRow(RowSet rowSet) throws Exception {
		FileData data = new FileData();
		LOGGER.info("Mapping FileData starts");
		if (rowSet.getColumnValue(0) != null && !rowSet.getColumnValue(0).isEmpty()) {
			data.setRuleName(rowSet.getColumnValue(0));
		} else {
			data.setRuleName(null);
		}
		if (rowSet.getColumnValue(1) != null && !rowSet.getColumnValue(1).isEmpty()) {
			data.setRuleDescription(rowSet.getColumnValue(1));
		} else {
			data.setRuleDescription(null);
		}
		if (rowSet.getColumnValue(2) != null && !rowSet.getColumnValue(2).isEmpty()) {
			data.setStartDate(rowSet.getColumnValue(2));
		} else {
			data.setStartDate(null);
		}
		if (rowSet.getColumnValue(3) != null && !rowSet.getColumnValue(3).isEmpty()) {
			data.setEndDate(rowSet.getColumnValue(3));
		} else {
			data.setEndDate(null);
		}
		if (rowSet.getColumnValue(4) != null && !rowSet.getColumnValue(4).isEmpty()) {
			data.setServiceProvider(rowSet.getColumnValue(4));
		} else {
			data.setServiceProvider(null);
		}
		if (rowSet.getColumnValue(5) != null && !rowSet.getColumnValue(5).isEmpty()) {
			data.setPaCoverage(rowSet.getColumnValue(5));
		} else {
			data.setPaCoverage(null);
		}
		if (rowSet.getColumnValue(6) != null && !rowSet.getColumnValue(6).isEmpty()) {
			data.setSsaCoverage(rowSet.getColumnValue(6));
		} else {
			data.setSsaCoverage(null);
		}
		if (rowSet.getColumnValue(7) != null && !rowSet.getColumnValue(7).isEmpty()) {
			data.setMinDaysPurchase(rowSet.getColumnValue(7));
		} else {
			data.setMinDaysPurchase(null);
		}
		if (rowSet.getColumnValue(8) != null && !rowSet.getColumnValue(8).isEmpty()) {
			data.setMaxDaysPurchase(rowSet.getColumnValue(8));
		} else {
			data.setMaxDaysPurchase(null);
		}
		if (rowSet.getColumnValue(9) != null && !rowSet.getColumnValue(9).isEmpty()) {
			data.setMinDaysInstall(rowSet.getColumnValue(9));
		} else {
			data.setMinDaysInstall(null);
		}
		if (rowSet.getColumnValue(10) != null && !rowSet.getColumnValue(10).isEmpty()) {
			data.setMaxDaysInstall(rowSet.getColumnValue(10));
		} else {
			data.setMaxDaysInstall(null);
		}
		if (rowSet.getColumnValue(11) != null && !rowSet.getColumnValue(11).isEmpty()) {
			data.setMinAttemptNumber(rowSet.getColumnValue(11));
		} else {
			data.setMinAttemptNumber(null);
		}
		if (rowSet.getColumnValue(12) != null && !rowSet.getColumnValue(12).isEmpty()) {
			data.setMaxAttemptNumber(rowSet.getColumnValue(12));
		} else {
			data.setMaxAttemptNumber(null);
		}
		if (rowSet.getColumnValue(13) != null && !rowSet.getColumnValue(13).isEmpty()) {
			data.setMinDaysW2(rowSet.getColumnValue(13));
		} else {
			data.setMinDaysW2(null);
		}
		if (rowSet.getColumnValue(14) != null && !rowSet.getColumnValue(14).isEmpty()) {
			data.setW2Offset(rowSet.getColumnValue(14));
		} else {
			data.setW2Offset(null);
		}
		if (rowSet.getColumnValue(15) != null && !rowSet.getColumnValue(15).isEmpty()) {
			data.setBrands(rowSet.getColumnValue(15));
		} else {
			data.setBrands(null);
		}
		if (rowSet.getColumnValue(16) != null && !rowSet.getColumnValue(16).isEmpty()) {
			data.setClients(rowSet.getColumnValue(16));
		} else {
			data.setClients(null);
		}
		if (rowSet.getColumnValue(17) != null && !rowSet.getColumnValue(17).isEmpty()) {
			data.setCoverage(rowSet.getColumnValue(17));
		} else {
			data.setCoverage(null);
		}
		if (rowSet.getColumnValue(18) != null && !rowSet.getColumnValue(18).isEmpty()) {
			data.setCreatorsUnit(rowSet.getColumnValue(18));
		} else {
			data.setCreatorsUnit(null);
		}
		if (rowSet.getColumnValue(19) != null && !rowSet.getColumnValue(19).isEmpty()) {
			data.setCreatordId(rowSet.getColumnValue(19));
		} else {
			data.setCreatordId(null);
		}
		if (rowSet.getColumnValue(20) != null && !rowSet.getColumnValue(20).isEmpty()) {
			data.setHelpers(rowSet.getColumnValue(20));
		} else {
			data.setHelpers(null);
		}
		if (rowSet.getColumnValue(21) != null && !rowSet.getColumnValue(21).isEmpty()) {
			data.setPartStatus(rowSet.getColumnValue(21));
		} else {
			data.setPartStatus(null);
		}
		if (rowSet.getColumnValue(22) != null && !rowSet.getColumnValue(22).isEmpty()) {
			data.setProcessId(rowSet.getColumnValue(22));
		} else {
			data.setProcessId(null);
		}
		if (rowSet.getColumnValue(23) != null && !rowSet.getColumnValue(23).isEmpty()) {
			data.setProducts(rowSet.getColumnValue(23));
		} else {
			data.setProducts(null);
		}
		if (rowSet.getColumnValue(24) != null && !rowSet.getColumnValue(24).isEmpty()) {
			data.setProductMerchandise(rowSet.getColumnValue(24));
		} else {
			data.setProductMerchandise(null);
		}
		if (rowSet.getColumnValue(25) != null && !rowSet.getColumnValue(25).isEmpty()) {
			data.setServiceFlag(rowSet.getColumnValue(25));
		} else {
			data.setServiceFlag(null);
		}
		if (rowSet.getColumnValue(26) != null && !rowSet.getColumnValue(26).isEmpty()) {
			data.setServiceType(rowSet.getColumnValue(26));
		} else {
			data.setServiceType(null);
		}
		if (rowSet.getColumnValue(27) != null && !rowSet.getColumnValue(27).isEmpty()) {
			data.setSystemChannel(rowSet.getColumnValue(27));
		} else {
			data.setSystemChannel(null);
		}
		if (rowSet.getColumnValue(28) != null && !rowSet.getColumnValue(28).isEmpty()) {
			data.setZip(rowSet.getColumnValue(28));
		} else {
			data.setZip(null);
		}
		if (rowSet.getColumnValue(29) != null && !rowSet.getColumnValue(29).isEmpty()) {
			data.setIru(rowSet.getColumnValue(29));
		} else {
			data.setIru(null);
		}
		if (rowSet.getColumnValue(30) != null && !rowSet.getColumnValue(30).isEmpty()) {
			data.setFru(rowSet.getColumnValue(30));
		} else {
			data.setFru(null);
		}
		if (rowSet.getColumnValue(31) != null && !rowSet.getColumnValue(31).isEmpty()) {
			data.setUnit(rowSet.getColumnValue(31));
		} else {
			data.setUnit(null);
		}
		LOGGER.info("Mapping FileData: {}  end ", data);
		return data;
	}

}
