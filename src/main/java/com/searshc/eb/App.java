package com.searshc.eb;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {


	public static void main(String[] args) throws Exception {
		
		String[] springConfig = { "loadfulfillmentRuleJob-context.xml" };

		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);

		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");

		Job job = (Job) context.getBean("loadfulfillmentRule");

		JobExecution execution = jobLauncher.run(job, new JobParameters());
		System.out.println("ExitStatus " + execution.getStatus());
		System.out.println(execution.getAllFailureExceptions());
		System.out.println(execution.getStepExecutions());

	}

}
