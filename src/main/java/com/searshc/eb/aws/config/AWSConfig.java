package com.searshc.eb.aws.config;


public final class AWSConfig {
	
	//aws configure for local testing
	public static final String USER_KEY = "";
	public static final String USER_SECRET = "";
	//public static final String TEMPLATE_NAME = "WorkerBatchTemplate";
	// for testing purpose both are same
	public static final String SENDER_EMAIL = "HS_SRD_TEAM@searshc.com";
	public static final String FAILED_RECEVIER_EMAIL = "SHI_HS_PSOM@searshc.com";
	public static final String SUCCESS_RECEVIER_EMAIL = "SHI_HS_PSOM@searshc.com";
	public static final String REGION_NAME = "us-east-1";
	

}
