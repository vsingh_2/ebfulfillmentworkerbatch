package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_helper_type")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="HelperTypeLookup")*/
public class HelperTypeLookup extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3975721986277918200L;
	private String helperTypeId;
	private String helperTypeCode;
	private String helperTypeDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "helper_type_id", nullable = false, unique = true)
	public String getHelperTypeId() {
		return helperTypeId;
	}

	public void setHelperTypeId(String helperTypeId) {
		this.helperTypeId = helperTypeId;
	}

	@Column(name = "helper_type_code", nullable = false, length = 6, unique = true)
	public String getHelperTypeCode() {
		return helperTypeCode;
	}

	public void setHelperTypeCode(String helperTypeCode) {
		this.helperTypeCode = helperTypeCode;
	}

	@Column(name = "helper_type_description", nullable = false, length = 60)
	public String getHelperTypeDescription() {
		return helperTypeDescription;
	}

	public void setHelperTypeDescription(String helperTypeDescription) {
		this.helperTypeDescription = helperTypeDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("helperTypeId", helperTypeId);
		builder.append("helperTypeCode", helperTypeCode);
		builder.append("helperTypeDescription", helperTypeDescription);
		return builder.toString();
	}
}
