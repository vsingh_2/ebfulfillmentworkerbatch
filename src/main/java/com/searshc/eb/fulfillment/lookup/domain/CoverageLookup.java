package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_coverage")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="CoverageLookup")*/
public class CoverageLookup extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8738022804750861481L;
	private String coverageId;
	private String coverageCode;
	private String coverageDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "coverage_id", nullable = false, unique = true)
	public String getCoverageId() {
		return coverageId;
	}

	public void setCoverageId(String coverageId) {
		this.coverageId = coverageId;
	}

	@Column(name = "coverage_code", nullable = false, length = 2, unique = true)
	public String getCoverageCode() {
		return coverageCode;
	}

	public void setCoverageCode(String coverageCode) {
		this.coverageCode = coverageCode;
	}

	@Column(name = "coverage_description", nullable = false, length = 25)
	public String getCoverageDescription() {
		return coverageDescription;
	}

	public void setCoverageDescription(String coverageDescription) {
		this.coverageDescription = coverageDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("coverageId", coverageId);
		builder.append("coverageCode", coverageCode);
		builder.append("coverageDescription", coverageDescription);
		return builder.toString();
	}
}
