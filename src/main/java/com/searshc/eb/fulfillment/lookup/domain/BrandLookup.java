package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_brands")
public class BrandLookup extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6104157235968698407L;
	private String brandId;
	private String brandName;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "brand_id", nullable = false, unique = true)
	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	@Column(name = "brand_name", nullable = false, length = 12, unique = true)
	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("brandId", brandId);
		builder.append("brandName", brandName);
		return builder.toString();
	}
}
