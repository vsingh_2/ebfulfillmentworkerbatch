package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_merch_types")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="MerchandiseTypeLookup")*/
public class MerchandiseTypeLookup extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4702033223652066292L;
	private String merchTypeId;
	private String merchandiseTypeCode;
	private String merchandiseTypeDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "merch_type_id", nullable = false, unique = true)
	public String getMerchTypeId() {
		return merchTypeId;
	}

	public void setMerchTypeId(String merchTypeId) {
		this.merchTypeId = merchTypeId;
	}

	@Column(name = "merch_type_code", nullable = false, length = 6)
	public String getMerchandiseTypeCode() {
		return merchandiseTypeCode;
	}

	public void setMerchandiseTypeCode(String merchandiseTypeCode) {
		this.merchandiseTypeCode = merchandiseTypeCode;
	}

	@Column(name = "merch_type_description", nullable = false, length = 60)
	public String getMerchandiseTypeDescription() {
		return merchandiseTypeDescription;
	}

	public void setMerchandiseTypeDescription(String merchandiseTypeDescription) {
		this.merchandiseTypeDescription = merchandiseTypeDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("merchTypeId", merchTypeId);
		builder.append("merchandiseTypeCode", merchandiseTypeCode);
		builder.append("merchandiseTypeDescription", merchandiseTypeDescription);
		return builder.toString();
	}
}
