package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_channel")
public class ChannelLookup extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7289985320988497943L;
	private String channelId;
	private String channelCode;
	private String channelDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "channel_id", nullable = false, unique = true)
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	@Column(name = "channel_code", nullable = false, length = 2, unique = true)
	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	@Column(name = "channel_description", nullable = false, length = 25)
	public String getChannelDescription() {
		return channelDescription;
	}

	public void setChannelDescription(String channelDescription) {
		this.channelDescription = channelDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("channelId", channelId);
		builder.append("channelCode", channelCode);
		builder.append("channelDescription", channelDescription);
		return builder.toString();
	}
}
