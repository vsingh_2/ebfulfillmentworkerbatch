package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_part_status_type")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="PartStatusTypeLookup")*/
public class PartStatusTypeLookup extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8670819565929829427L;
	private String partStatusId;
	private String partStatusCode;
	private String partStatusDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "prt_status_id", nullable = false, unique = true)
	public String getPartStatusId() {
		return partStatusId;
	}

	public void setPartStatusId(String partStatusId) {
		this.partStatusId = partStatusId;
	}

	@Column(name = "prt_status_code", nullable = false, length = 2)
	public String getPartStatusCode() {
		return partStatusCode;
	}

	public void setPartStatusCode(String partStatusCode) {
		this.partStatusCode = partStatusCode;
	}

	@Column(name = "prt_status_description", nullable = false, length = 25)
	public String getPartStatusDescription() {
		return partStatusDescription;
	}

	public void setPartStatusDescription(String partStatusDescription) {
		this.partStatusDescription = partStatusDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("partStatusId", partStatusId);
		builder.append("partStatusCode", partStatusCode);
		builder.append("partStatusDescription", partStatusDescription);
		return builder.toString();
	}
}
