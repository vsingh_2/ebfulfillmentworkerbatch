package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_svc_types")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="ServiceTypeLookup")*/
public class ServiceTypeLookup extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = 167681258272296761L;
	private String serviceTypeId;
	private String serviceTypeCode;
	private String serviceTypeDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "svc_type_id", nullable = false, unique = true)
	public String getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(String serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	@Column(name = "svc_type_code", nullable = false, length = 6)
	public String getServiceTypeCode() {
		return serviceTypeCode;
	}

	public void setServiceTypeCode(String serviceTypeCode) {
		this.serviceTypeCode = serviceTypeCode;
	}

	@Column(name = "svc_type_description", nullable = false, length = 60)
	public String getServiceTypeDescription() {
		return serviceTypeDescription;
	}

	public void setServiceTypeDescription(String serviceTypeDescription) {
		this.serviceTypeDescription = serviceTypeDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("serviceTypeId", serviceTypeId);
		builder.append("serviceTypeCode", serviceTypeCode);
		builder.append("serviceTypeDescription", serviceTypeDescription);
		return builder.toString();
	}
}
