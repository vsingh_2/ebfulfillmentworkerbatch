package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_service_providers")
public class ServiceProviderLookup extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 512112781639530639L;
	private String servicerId;
	private String servicerName;
	private String servicerUrl;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "servicer_id", nullable = false, unique = true)
	public String getServicerId() {
		return servicerId;
	}

	public void setServicerId(String servicerId) {
		this.servicerId = servicerId;
	}

	@Column(name = "servicer_name", nullable = false, length = 255)
	public String getServicerName() {
		return servicerName;
	}

	public void setServicerName(String servicerName) {
		this.servicerName = servicerName;
	}

	@Column(name = "servicer_url", nullable = false, length = 255)
	public String getServicerUrl() {
		return servicerUrl;
	}

	public void setServicerUrl(String servicerUrl) {
		this.servicerUrl = servicerUrl;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("servicerId", servicerId);
		builder.append("servicerName", servicerName);
		builder.append("servicerUrl", servicerUrl);
		return builder.toString();
	}
}
