package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_geo_types")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="GeographyTypeLookup")*/
public class GeographyTypeLookup extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3435080928899601465L;
	private String geoTypeId;
	private String geoTypeCode;
	private String geoTypeDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "geo_type_id", nullable = false, unique = true)
	public String getGeoTypeId() {
		return geoTypeId;
	}

	public void setGeoTypeId(String geoTypeId) {
		this.geoTypeId = geoTypeId;
	}

	@Column(name = "geo_type_code", nullable = false, length = 6, unique = true)
	public String getGeoTypeCode() {
		return geoTypeCode;
	}

	public void setGeoTypeCode(String geoTypeCode) {
		this.geoTypeCode = geoTypeCode;
	}

	@Column(name = "geo_type_description", nullable = false, length = 60)
	public String getGeoTypeDescription() {
		return geoTypeDescription;
	}

	public void setGeoTypeDescription(String geoTypeDescription) {
		this.geoTypeDescription = geoTypeDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("geoTypeId", geoTypeId);
		builder.append("geoTypeCode", geoTypeCode);
		builder.append("geoTypeDescription", geoTypeDescription);
		return builder.toString();
	}
}
