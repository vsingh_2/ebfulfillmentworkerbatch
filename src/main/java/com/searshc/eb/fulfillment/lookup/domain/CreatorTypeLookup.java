package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_creator_type")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="CreatorTypeLookup")*/
public class CreatorTypeLookup extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6721865651444098050L;
	private String reqCreatorTypeId;
	private String reqCreatorTypeDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "req_creator_type_id", nullable = false, unique = true)
	public String getReqCreatorTypeId() {
		return reqCreatorTypeId;
	}

	public void setReqCreatorTypeId(String reqCreatorTypeId) {
		this.reqCreatorTypeId = reqCreatorTypeId;
	}

	@Column(name = "req_creator_type_description", nullable = false, length = 25)
	public String getReqCreatorTypeDescription() {
		return reqCreatorTypeDescription;
	}

	public void setReqCreatorTypeDescription(String reqCreatorTypeDescription) {
		this.reqCreatorTypeDescription = reqCreatorTypeDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("reqCreatorTypeId", reqCreatorTypeId);
		builder.append("reqCreatorTypeDescription", reqCreatorTypeDescription);
		return builder.toString();
	}
}
