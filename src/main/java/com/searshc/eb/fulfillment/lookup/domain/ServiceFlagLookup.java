package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_svc_flags")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="ServiceFlagLookup")*/
public class ServiceFlagLookup extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2958049511389644889L;
	private String serviceFlagId;
	private String serviceFlagCode;
	private String serviceFlagDescription;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "svc_flag_id", nullable = false, unique = true)
	public String getServiceFlagId() {
		return serviceFlagId;
	}

	public void setServiceFlagId(String serviceFlagId) {
		this.serviceFlagId = serviceFlagId;
	}

	@Column(name = "svc_flag_code", nullable = false, length = 6)
	public String getServiceFlagCode() {
		return serviceFlagCode;
	}

	public void setServiceFlagCode(String serviceFlagCode) {
		this.serviceFlagCode = serviceFlagCode;
	}

	@Column(name = "service_flag_description", nullable = false, length = 60)
	public String getServiceFlagDescription() {
		return serviceFlagDescription;
	}

	public void setServiceFlagDescription(String serviceFlagDescription) {
		this.serviceFlagDescription = serviceFlagDescription;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("serviceFlagId", serviceFlagId);
		builder.append("serviceFlagCode", serviceFlagCode);
		builder.append("serviceFlagDescription", serviceFlagDescription);
		return builder.toString();
	}

}
