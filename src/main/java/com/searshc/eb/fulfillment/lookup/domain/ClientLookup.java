package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_clients")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="ClientLookup")*/
public class ClientLookup extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4697284940520137512L;
	private String clientId;
	private String clientCode;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "client_id", nullable = false, unique = true)
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Column(name = "client_code", nullable = false, length = 6, unique = true)
	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("clientId", clientId);
		builder.append("clientCode", clientCode);
		return builder.toString();
	}

}
