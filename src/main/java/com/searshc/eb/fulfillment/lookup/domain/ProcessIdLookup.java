package com.searshc.eb.fulfillment.lookup.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.domain.BaseDomain;

@Entity
@Table(name = "eb_lu_process_id")
/*@Cache(usage=CacheConcurrencyStrategy.READ_ONLY, 
region="ProcessIdLookup")*/
public class ProcessIdLookup extends BaseDomain{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6407266407801131741L;
	private String processId;
	private String processIdCode;
	private String processIdDescription;
	private Date endDate;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "process_id", nullable = false, unique = true)
	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	@Column(name = "process_id_code", nullable = false, length = 6, unique = true)
	public String getProcessIdCode() {
		return processIdCode;
	}

	public void setProcessIdCode(String processIdCode) {
		this.processIdCode = processIdCode;
	}

	@Column(name = "process_id_description", nullable = false, length = 60)
	public String getProcessIdDescription() {
		return processIdDescription;
	}

	public void setProcessIdDescription(String processIdDescription) {
		this.processIdDescription = processIdDescription;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date", nullable = false, length = 10)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("processId", processId);
		builder.append("processIdCode", processIdCode);
		builder.append("processIdDescription", processIdDescription);
		builder.append("endDate", endDate);
		return builder.toString();
	}
}
