package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.BrandLookup;

@Entity
@Table(name = "eb_fulfillment_brand")
public class Brand extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -859914835099795824L;
	private String brandId;
	private Rule rule;
	private BrandLookup brandLookup;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_brand_id", nullable = false, unique = true)
	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "brand_id", nullable = false)
	public BrandLookup getBrandLookup() {
		return brandLookup;
	}

	public void setBrandLookup(BrandLookup brandLookup) {
		this.brandLookup = brandLookup;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("brandId", brandId);
		builder.append("rule", rule);
		builder.append("brandLookup", brandLookup);
		return builder.toString();
	}
}
