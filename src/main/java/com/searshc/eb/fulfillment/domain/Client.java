package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.searshc.eb.fulfillment.lookup.domain.ClientLookup;

@Entity
@Table(name = "eb_fulfillment_client")
public class Client extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1915076642673907562L;
	private String clientId;
	private Rule rule;
	private ClientLookup client;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_client_id", nullable = false, unique = true)
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	@OnDelete(action = OnDeleteAction.CASCADE)
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "client_id", nullable = false)
	public ClientLookup getClient() {
		return client;
	}

	public void setClient(ClientLookup client) {
		this.client = client;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("clientId", clientId);
		builder.append("rule", rule);
		builder.append("client", client);
		return builder.toString();
	}
}
