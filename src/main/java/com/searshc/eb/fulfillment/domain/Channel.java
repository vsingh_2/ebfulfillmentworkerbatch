package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.ChannelLookup;

@Entity
@Table(name = "eb_fulfillment_channel")
public class Channel extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4627554137169627202L;
	private String channelId;
	private Rule rule;
	private ChannelLookup channelLookup;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_channel_id", nullable = false, unique = true)
	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "channel_id", nullable = false)
	public ChannelLookup getChannelLookup() {
		return channelLookup;
	}

	public void setChannelLookup(ChannelLookup channelLookup) {
		this.channelLookup = channelLookup;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("channelId", channelId);
		builder.append("rule", rule);
		builder.append("channelLookup", channelLookup);
		return builder.toString();
	}
}
