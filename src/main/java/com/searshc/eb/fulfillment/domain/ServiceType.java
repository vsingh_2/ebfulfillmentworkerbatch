package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.ServiceTypeLookup;

@Entity
@Table(name = "eb_fulfillment_svc_type")
public class ServiceType extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2339639174333793826L;
	private String serviceTypeId;
	private Rule rule;
	private ServiceTypeLookup serviceTypeLookup;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_svc_type_id", nullable = false, unique = true)
	public String getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(String serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "svc_type_id", nullable = false)
	public ServiceTypeLookup getServiceTypeLookup() {
		return serviceTypeLookup;
	}

	public void setServiceTypeLookup(ServiceTypeLookup serviceTypeLookup) {
		this.serviceTypeLookup = serviceTypeLookup;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("serviceTypeId", serviceTypeId);
		builder.append("rule", rule);
		builder.append("serviceTypeLookup", serviceTypeLookup);
		return builder.toString();
	}
}
