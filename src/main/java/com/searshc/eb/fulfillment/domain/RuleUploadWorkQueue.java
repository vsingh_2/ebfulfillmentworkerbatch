package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@Entity
@Table(name = "eb_fulfillment_rule_upload_workqueue")
public class RuleUploadWorkQueue extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3698855975444806235L;
	private String requestWorkqueueId;
	private String userFileName;
	private String systemFilePath;
	private char statusCode;
	private String requestSubmittedBy;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "request_workqueue_id", nullable = false, unique = true)
	public String getRequestWorkqueueId() {
		return requestWorkqueueId;
	}

	public void setRequestWorkqueueId(String requestWorkqueueId) {
		this.requestWorkqueueId = requestWorkqueueId;
	}

	@Column(name = "user_file_name", nullable = false, length = 100)
	public String getUserFileName() {
		return userFileName;
	}

	public void setUserFileName(String userFileName) {
		this.userFileName = userFileName;
	}

	@Column(name = "system_file_path", nullable = false, length = 100)
	public String getSystemFilePath() {
		return systemFilePath;
	}

	public void setSystemFilePath(String systemFilePath) {
		this.systemFilePath = systemFilePath;
	}

	@Column(name = "status_cd", nullable = false, length = 1)
	public char getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(char statusCode) {
		this.statusCode = statusCode;
	}

	@Column(name = "request_submitted_by", nullable = false, length = 45)
	public String getRequestSubmittedBy() {
		return requestSubmittedBy;
	}

	public void setRequestSubmittedBy(String requestSubmittedBy) {
		this.requestSubmittedBy = requestSubmittedBy;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("requestWorkqueueId", requestWorkqueueId);
		builder.append("userFileName", userFileName);
		builder.append("systemFilePath", systemFilePath);
		builder.append("statusCode", statusCode);
		builder.append("requestSubmittedBy", requestSubmittedBy);
		return builder.toString();
	}
}
