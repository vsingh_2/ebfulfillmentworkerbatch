package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.GeographyTypeLookup;

@Entity
@Table(name = "eb_fulfillment_geo")
public class Geography extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5189996708440194L;
	private String geographyId;
	private Rule rule;
	private GeographyTypeLookup geoType;
	private String geoValue;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_geo_id", nullable = false, unique = true)
	public String getGeographyId() {
		return geographyId;
	}

	public void setGeographyId(String geographyId) {
		this.geographyId = geographyId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "geo_type_id", nullable = false)
	public GeographyTypeLookup getGeoType() {
		return geoType;
	}

	public void setGeoType(GeographyTypeLookup geoType) {
		this.geoType = geoType;
	}

	@Column(name = "geo_value", nullable = false, length = 10)
	public String getGeoValue() {
		return geoValue;
	}

	public void setGeoValue(String geoValue) {
		this.geoValue = geoValue;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("geographyId", geographyId);
		builder.append("rule", rule);
		builder.append("geoType", geoType);
		builder.append("geoValue", geoValue);
		return builder.toString();
	}
}
