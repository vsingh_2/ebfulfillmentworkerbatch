package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.searshc.eb.fulfillment.lookup.domain.ServiceProviderLookup;

@Entity
@Table(name = "eb_fulfillment_rule")
public class Rule extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3810668760802324681L;
	private String ruleId;
	private String ruleName;
	private String ruleDescription;
	private Date startDate;
	private Date endDate;
	private String paCoverageFlag;
	private String ssaCoverageFlag;
	private Integer minDaysFromPurchase;
	private Integer maxDaysFromPurchase;
	private Integer minDaysFromInstallation;
	private Integer maxDaysFromInstallation;
	private Integer minAttemptNum;
	private Integer maxAttemptNum;
	private Integer minDaysToW2;
	private Integer maxDaysToW2;
	private Integer w2Offset;
	private boolean uploadFlag;
	private boolean protectedFlag;
	private ServiceProviderLookup provider;
	private RulePriority priority;
	private List<ServiceType> serviceTypes;
	private List<Geography> geos;
	private List<Product> products;
	private List<ServiceFlags> serviceFlags;
	private List<Client> clients;
	private List<Brand> brands;
	private List<Helper> helpers;
	private List<ProcessId> procs;
	private List<Creator> creators;
	private List<PartStatus> partStatuses;
	private List<Channel> channels;
	private List<Coverage> coverages;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "fulfillment_rule_id", nullable = false, unique = true)
	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	@Column(name = "rule_name", nullable = false, length = 50, unique = true)
	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	@Column(name = "rule_description", nullable = false, length = 255)
	public String getRuleDescription() {
		return ruleDescription;
	}

	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "rule_start_date", nullable = false, length = 10)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "rule_end_date", nullable = false, length = 10)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "pa_coverage_flag", nullable = true, length = 1)
	public String getPaCoverageFlag() {
		return paCoverageFlag;
	}

	public void setPaCoverageFlag(String paCoverageFlag) {
		this.paCoverageFlag = paCoverageFlag;
	}

	@Column(name = "ssa_coverage_flag", nullable = true, length = 1)
	public String getSsaCoverageFlag() {
		return ssaCoverageFlag;
	}

	public void setSsaCoverageFlag(String ssaCoverageFlag) {
		this.ssaCoverageFlag = ssaCoverageFlag;
	}

	@Column(name = "min_days_from_purchase", nullable = true, length = 11)
	public Integer getMinDaysFromPurchase() {
		return minDaysFromPurchase;
	}

	public void setMinDaysFromPurchase(Integer minDaysFromPurchase) {
		this.minDaysFromPurchase = minDaysFromPurchase;
	}

	@Column(name = "max_days_from_purchase", nullable = true, length = 11)
	public Integer getMaxDaysFromPurchase() {
		return maxDaysFromPurchase;
	}

	public void setMaxDaysFromPurchase(Integer maxDaysFromPurchase) {
		this.maxDaysFromPurchase = maxDaysFromPurchase;
	}

	@Column(name = "min_days_from_installation", nullable = true, length = 11)
	public Integer getMinDaysFromInstallation() {
		return minDaysFromInstallation;
	}

	public void setMinDaysFromInstallation(Integer minDaysFromInstallation) {
		this.minDaysFromInstallation = minDaysFromInstallation;
	}

	@Column(name = "max_days_from_installation", nullable = true, length = 11)
	public Integer getMaxDaysFromInstallation() {
		return maxDaysFromInstallation;
	}

	public void setMaxDaysFromInstallation(Integer maxDaysFromInstallation) {
		this.maxDaysFromInstallation = maxDaysFromInstallation;
	}

	@Column(name = "min_attempt_num", nullable = true, length = 11)
	public Integer getMinAttemptNum() {
		return minAttemptNum;
	}

	public void setMinAttemptNum(Integer minAttemptNum) {
		this.minAttemptNum = minAttemptNum;
	}

	@Column(name = "max_attempt_num", nullable = true, length = 11)
	public Integer getMaxAttemptNum() {
		return maxAttemptNum;
	}

	public void setMaxAttemptNum(Integer maxAttemptNum) {
		this.maxAttemptNum = maxAttemptNum;
	}

	@Column(name = "min_days_to_w2", nullable = true, length = 11)
	public Integer getMinDaysToW2() {
		return minDaysToW2;
	}

	public void setMinDaysToW2(Integer minDaysToW2) {
		this.minDaysToW2 = minDaysToW2;
	}

	@Column(name = "max_days_to_w2", nullable = true, length = 11)
	public Integer getMaxDaysToW2() {
		return maxDaysToW2;
	}

	public void setMaxDaysToW2(Integer maxDaysToW2) {
		this.maxDaysToW2 = maxDaysToW2;
	}

	@Column(name = "w2_offset", nullable = true, length = 11)
	public Integer getW2Offset() {
		return w2Offset;
	}

	public void setW2Offset(Integer w2Offset) {
		this.w2Offset = w2Offset;
	}

	@Column(name = "upload_flag", nullable = true, length = 1)
	public boolean isUploadFlag() {
		return uploadFlag;
	}

	public void setUploadFlag(boolean uploadFlag) {
		this.uploadFlag = uploadFlag;
	}

	@Column(name = "protected_flag", nullable = false, length = 1)
	public boolean isProtectedFlag() {
		return protectedFlag;
	}

	public void setProtectedFlag(boolean protectedFlag) {
		this.protectedFlag = protectedFlag;
	}

	@OneToOne
	@JoinColumn(name = "servicer_id", nullable = false)
	public ServiceProviderLookup getProvider() {
		return provider;
	}

	public void setProvider(ServiceProviderLookup provider) {
		this.provider = provider;
	}

	@OneToOne(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public RulePriority getPriority() {
		return priority;
	}

	public void setPriority(RulePriority priority) {
		this.priority = priority;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<ServiceType> getServiceTypes() {
		return serviceTypes;
	}

	public void setServiceTypes(List<ServiceType> serviceTypes) {
		this.serviceTypes = serviceTypes;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<Geography> getGeos() {
		return geos;
	}

	public void setGeos(List<Geography> geos) {
		this.geos = geos;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<ServiceFlags> getServiceFlags() {
		return serviceFlags;
	}

	public void setServiceFlags(List<ServiceFlags> serviceFlags) {
		this.serviceFlags = serviceFlags;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<Brand> getBrands() {
		return brands;
	}

	public void setBrands(List<Brand> brands) {
		this.brands = brands;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<Helper> getHelpers() {
		return helpers;
	}

	public void setHelpers(List<Helper> helpers) {
		this.helpers = helpers;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<ProcessId> getProcs() {
		return procs;
	}

	public void setProcs(List<ProcessId> procs) {
		this.procs = procs;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<Creator> getCreators() {
		return creators;
	}

	public void setCreators(List<Creator> creators) {
		this.creators = creators;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<PartStatus> getPartStatuses() {
		return partStatuses;
	}

	public void setPartStatuses(List<PartStatus> partStatuses) {
		this.partStatuses = partStatuses;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<Channel> getChannels() {
		return channels;
	}

	public void setChannels(List<Channel> channels) {
		this.channels = channels;
	}

	@OneToMany(mappedBy = "rule", orphanRemoval = true)
	@Cascade(CascadeType.ALL)
	public List<Coverage> getCoverages() {
		return coverages;
	}

	public void setCoverages(List<Coverage> coverages) {
		this.coverages = coverages;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("ruleId", ruleId);
		builder.append("ruleName", ruleName);
		builder.append("ruleDescription", ruleDescription);
		builder.append("startDate", startDate);
		builder.append("endDate", endDate);
		builder.append("paCoverageFlag", paCoverageFlag);
		builder.append("ssaCoverageFlag", ssaCoverageFlag);
		builder.append("minDaysFromPurchase", minDaysFromPurchase);
		builder.append("maxDaysFromPurchase", maxDaysFromPurchase);
		builder.append("minDaysFromInstallation", minDaysFromInstallation);
		builder.append("maxDaysFromInstallation", maxDaysFromInstallation);
		builder.append("minAttemptNum", minAttemptNum);
		builder.append("maxAttemptNum", maxAttemptNum);
		builder.append("minDaysToW2", minDaysToW2);
		builder.append("maxDaysToW2", maxDaysToW2);
		builder.append("w2Offset", w2Offset);
		builder.append("uploadFlag", uploadFlag);
		builder.append("protectedFlag", protectedFlag);
		builder.append("provider", provider);
		builder.append("priority", priority);
		builder.append("serviceTypes", serviceTypes);
		builder.append("geos", geos);
		builder.append("products", products);
		builder.append("serviceFlags", serviceFlags);
		builder.append("clients", clients);
		builder.append("brands", brands);
		builder.append("helpers", helpers);
		builder.append("procs", procs);
		builder.append("creators", creators);
		builder.append("partStatuses", partStatuses);
		builder.append("channels", channels);
		builder.append("coverages", coverages);
		return builder.toString();
	}
}
