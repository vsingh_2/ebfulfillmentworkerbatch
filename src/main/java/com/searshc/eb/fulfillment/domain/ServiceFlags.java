package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.ServiceFlagLookup;

@Entity
@Table(name = "eb_fulfillment_svc_flags")
public class ServiceFlags extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2407976345066760146L;
	private String serviceFlagsId;
	private Rule rule;
	private ServiceFlagLookup serviceFlagLookup;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_svc_flags_id", nullable = false, unique = true)
	public String getServiceFlagsId() {
		return serviceFlagsId;
	}

	public void setServiceFlagsId(String serviceFlagsId) {
		this.serviceFlagsId = serviceFlagsId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "svc_flag_id", nullable = false)
	public ServiceFlagLookup getServiceFlagLookup() {
		return serviceFlagLookup;
	}

	public void setServiceFlagLookup(ServiceFlagLookup serviceFlagLookup) {
		this.serviceFlagLookup = serviceFlagLookup;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("serviceFlagsId", serviceFlagsId);
		builder.append("rule", rule);
		builder.append("serviceFlagLookup", serviceFlagLookup);
		return builder.toString();
	}
}
