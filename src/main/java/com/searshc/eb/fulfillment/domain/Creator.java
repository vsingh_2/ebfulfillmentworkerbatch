package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.CreatorTypeLookup;

@Entity
@Table(name = "eb_fulfillment_creator")
public class Creator extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5091234797489181423L;
	private String ffCreatorId;
	private Rule rule;
	private CreatorTypeLookup creatorType;
	private String reqCreatorId;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_creator_id", nullable = false, unique = true)
	public String getFfCreatorId() {
		return ffCreatorId;
	}

	public void setFfCreatorId(String ffCreatorId) {
		this.ffCreatorId = ffCreatorId;
	}

	@Column(name = "req_creator_id", nullable = false, length = 25)
	public String getReqCreatorId() {
		return reqCreatorId;
	}

	public void setReqCreatorId(String reqCreatorId) {
		this.reqCreatorId = reqCreatorId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "req_creator_type_id", nullable = false)
	public CreatorTypeLookup getCreatorType() {
		return creatorType;
	}

	public void setCreatorType(CreatorTypeLookup creatorType) {
		this.creatorType = creatorType;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("ffCreatorId", ffCreatorId);
		builder.append("reqCreatorId", reqCreatorId);
		builder.append("rule", rule);
		builder.append("creatorType", creatorType);
		return builder.toString();
	}
}
