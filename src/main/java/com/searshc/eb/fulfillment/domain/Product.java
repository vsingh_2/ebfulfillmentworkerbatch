package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.MerchandiseTypeLookup;

@Entity
@Table(name = "eb_fulfillment_product")
public class Product extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6553932775410058280L;
	private String productId;
	private Rule rule;
	private MerchandiseTypeLookup merchType;
	private String merchValue;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_product_id", nullable = false, unique = true)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	// Foreign Key
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	// Foreign Key
	@OneToOne
	@JoinColumn(name = "merch_type_id", nullable = false)
	public MerchandiseTypeLookup getMerchType() {
		return merchType;
	}

	public void setMerchType(MerchandiseTypeLookup merchType) {
		this.merchType = merchType;
	}

	@Column(name = "merch_value", nullable = false, length = 12)
	public String getMerchValue() {
		return merchValue;
	}

	public void setMerchValue(String merchValue) {
		this.merchValue = merchValue;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("productId", productId);
		builder.append("rule", rule);
		builder.append("merchType", merchType);
		builder.append("merchValue", merchValue);
		return builder.toString();
	}
}
