package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@Entity
@Table(name = "event_broker_lock")
public class EventBrokerLock extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7097603013886555779L;
	private String eventBrokerLockId;
	private String businessFlow;
	private String fulfillmentRuleId;
	private String capacityAreaPriorityId;
	private Date lockStartTime;
	private Date lockEndTime;
	private String lockByUser;
	private char lockByUserType;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "event_broker_lock_id", nullable = false, unique = true)
	public String getEventBrokerLockId() {
		return eventBrokerLockId;
	}

	public void setEventBrokerLockId(String eventBrokerLockId) {
		this.eventBrokerLockId = eventBrokerLockId;
	}

	@Column(name = "business_flow", nullable = false, length = 3)
	public String getBusinessFlow() {
		return businessFlow;
	}

	public void setBusinessFlow(String businessFlow) {
		this.businessFlow = businessFlow;
	}

	@Column(name = "fulfillment_rule_id", nullable = true)
	public String getFulfillmentRuleId() {
		return fulfillmentRuleId;
	}

	public void setFulfillmentRuleId(String fulfillmentRuleId) {
		this.fulfillmentRuleId = fulfillmentRuleId;
	}

	@Column(name = "capacity_area_priority_id", nullable = true)
	public String getCapacityAreaPriorityId() {
		return capacityAreaPriorityId;
	}

	public void setCapacityAreaPriorityId(String capacityAreaPriorityId) {
		this.capacityAreaPriorityId = capacityAreaPriorityId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lock_start_time", nullable = false)
	public Date getLockStartTime() {
		return lockStartTime;
	}

	public void setLockStartTime(Date lockStartTime) {
		this.lockStartTime = lockStartTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lock_end_time", nullable = true)
	public Date getLockEndTime() {
		return lockEndTime;
	}

	public void setLockEndTime(Date lockEndTime) {
		this.lockEndTime = lockEndTime;
	}

	@Column(name = "lock_by_user", nullable = false, length = 10)
	public String getLockByUser() {
		return lockByUser;
	}

	public void setLockByUser(String lockByUser) {
		this.lockByUser = lockByUser;
	}

	@Column(name = "lock_by_user_type", nullable = false, length = 1)
	public char getLockByUserType() {
		return lockByUserType;
	}

	public void setLockByUserType(char lockByUserType) {
		this.lockByUserType = lockByUserType;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("eventBrokerLockId", eventBrokerLockId);
		builder.append("businessFlow", businessFlow);
		builder.append("fulfillmentRuleId", fulfillmentRuleId);
		builder.append("capacityAreaPriorityId", capacityAreaPriorityId);
		builder.append("lockStartTime", lockStartTime);
		builder.append("lockEndTime", lockEndTime);
		builder.append("lockByUser", lockByUser);
		builder.append("lockByUserType", lockByUserType);
		return builder.toString();
	}

}
