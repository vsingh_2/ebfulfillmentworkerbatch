package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@Entity
@Table(name = "eb_fulfillment_priority")
public class RulePriority extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -826647897250456631L;
	private String rulePriorityId;
	private Rule rule;
	private Integer priority;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_priority_id", nullable = false, unique = true)
	public String getRulePriorityId() {
		return rulePriorityId;
	}

	public void setRulePriorityId(String rulePriorityId) {
		this.rulePriorityId = rulePriorityId;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id", nullable = false)
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@Column(name = "priority", nullable = false, length = 11)
	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("rulePriorityId", rulePriorityId);
		builder.append("rule", rule);
		builder.append("priority", priority);
		return builder.toString();
	}
}
