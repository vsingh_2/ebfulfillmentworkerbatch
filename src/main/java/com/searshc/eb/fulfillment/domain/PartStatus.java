package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.PartStatusTypeLookup;

@Entity
@Table(name = "eb_fulfillment_part_status")
public class PartStatus extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -73723058933299868L;
	private String partStatusId;
	private Rule rule;
	private PartStatusTypeLookup partLookup;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_prt_status_id", nullable = false, unique = true)
	public String getPartStatusId() {
		return partStatusId;
	}

	public void setPartStatusId(String partStatusId) {
		this.partStatusId = partStatusId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "prt_status_id", nullable = false)
	public PartStatusTypeLookup getPartLookup() {
		return partLookup;
	}

	public void setPartLookup(PartStatusTypeLookup partLookup) {
		this.partLookup = partLookup;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("partStatusId", partStatusId);
		builder.append("rule", rule);
		builder.append("partLookup", partLookup);
		return builder.toString();
	}
}
