package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@Entity
@Table(name = "eb_fulfillment_file_data")
public class FileData extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8355490037379955259L;
	private String filedataId;
	private String ruleName;
	private String ruleDescription;
	private String startDate;
	private String endDate;
	private String serviceProvider;
	private String paCoverage;
	private String ssaCoverage;
	private String minDaysPurchase;
	private String maxDaysPurchase;
	private String minDaysInstall;
	private String maxDaysInstall;
	private String minAttemptNumber;
	private String maxAttemptNumber;
	private String minDaysW2;
	private String maxDaysW2;
	private String w2Offset;
	private String brands;
	private String clients;
	private String coverage;
	private String creatorsUnit;
	private String creatordId;
	private String helpers;
	private String partStatus;
	private String processId;
	private String products;
	private String productMerchandise;
	private String serviceFlag;
	private String serviceType;
	private String systemChannel;
	private String zip;
	private String iru;
	private String fru;
	private String unit;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "file_data_id", nullable = false, unique = true)
	public String getFiledataId() {
		return filedataId;
	}

	public void setFiledataId(String filedataId) {
		this.filedataId = filedataId;
	}

	@Column(name = "rule_name", nullable = true, length = 50)
	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	@Column(name = "rule_description", nullable = true, length = 255)
	public String getRuleDescription() {
		return ruleDescription;
	}

	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}

	@Column(name = "rule_start_date", nullable = true, length = 255)
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@Column(name = "rule_end_date", nullable = true, length = 255)
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Column(name = "servicer_id", nullable = true, length = 10)
	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	@Column(name = "pa_coverage_flag", nullable = true, length = 1)
	public String getPaCoverage() {
		return paCoverage;
	}

	public void setPaCoverage(String paCoverage) {
		this.paCoverage = paCoverage;
	}

	@Column(name = "ssa_coverage_flag", nullable = true, length = 1)
	public String getSsaCoverage() {
		return ssaCoverage;
	}

	public void setSsaCoverage(String ssaCoverage) {
		this.ssaCoverage = ssaCoverage;
	}

	@Column(name = "min_days_from_purchase", nullable = true, length = 11)
	public String getMinDaysPurchase() {
		return minDaysPurchase;
	}

	public void setMinDaysPurchase(String minDaysPurchase) {
		this.minDaysPurchase = minDaysPurchase;
	}

	@Column(name = "max_days_from_purchase", nullable = true, length = 11)
	public String getMaxDaysPurchase() {
		return maxDaysPurchase;
	}

	public void setMaxDaysPurchase(String maxDaysPurchase) {
		this.maxDaysPurchase = maxDaysPurchase;
	}

	@Column(name = "min_days_from_installation", nullable = true, length = 11)
	public String getMinDaysInstall() {
		return minDaysInstall;
	}

	public void setMinDaysInstall(String minDaysInstall) {
		this.minDaysInstall = minDaysInstall;
	}

	@Column(name = "max_days_from_installation", nullable = true, length = 11)
	public String getMaxDaysInstall() {
		return maxDaysInstall;
	}

	public void setMaxDaysInstall(String maxDaysInstall) {
		this.maxDaysInstall = maxDaysInstall;
	}

	@Column(name = "min_attempt_num", nullable = true, length = 11)
	public String getMinAttemptNumber() {
		return minAttemptNumber;
	}

	public void setMinAttemptNumber(String minAttemptNumber) {
		this.minAttemptNumber = minAttemptNumber;
	}

	@Column(name = "max_attempt_num", nullable = true, length = 11)
	public String getMaxAttemptNumber() {
		return maxAttemptNumber;
	}

	public void setMaxAttemptNumber(String maxAttemptNumber) {
		this.maxAttemptNumber = maxAttemptNumber;
	}

	@Column(name = "min_days_to_w2", nullable = true, length = 11)
	public String getMinDaysW2() {
		return minDaysW2;
	}

	public void setMinDaysW2(String minDaysW2) {
		this.minDaysW2 = minDaysW2;
	}

	@Column(name = "max_days_to_w2", nullable = true, length = 11)
	public String getMaxDaysW2() {
		return maxDaysW2;
	}

	public void setMaxDaysW2(String maxDaysW2) {
		this.maxDaysW2 = maxDaysW2;
	}

	@Column(name = "w2_offset", nullable = true, length = 11)
	public String getW2Offset() {
		return w2Offset;
	}

	public void setW2Offset(String w2Offset) {
		this.w2Offset = w2Offset;
	}

	@Column(name = "brands", nullable = true)
	public String getBrands() {
		return brands;
	}

	public void setBrands(String brands) {
		this.brands = brands;
	}

	@Column(name = "clients", nullable = true)
	public String getClients() {
		return clients;
	}

	public void setClients(String clients) {
		this.clients = clients;
	}

	@Column(name = "coverage", nullable = true)
	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	@Column(name = "creatorsUnit", nullable = true)
	public String getCreatorsUnit() {
		return creatorsUnit;
	}

	public void setCreatorsUnit(String creatorsUnit) {
		this.creatorsUnit = creatorsUnit;
	}

	@Column(name = "creatordId", nullable = true)
	public String getCreatordId() {
		return creatordId;
	}

	public void setCreatordId(String creatordId) {
		this.creatordId = creatordId;
	}

	@Column(name = "helpers", nullable = true)
	public String getHelpers() {
		return helpers;
	}

	public void setHelpers(String helpers) {
		this.helpers = helpers;
	}

	@Column(name = "partStatus", nullable = true)
	public String getPartStatus() {
		return partStatus;
	}

	public void setPartStatus(String partStatus) {
		this.partStatus = partStatus;
	}

	@Column(name = "processId", nullable = true)
	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	@Column(name = "products", nullable = true)
	public String getProducts() {
		return products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	@Column(name = "productMerchandise", nullable = true)
	public String getProductMerchandise() {
		return productMerchandise;
	}

	public void setProductMerchandise(String productMerchandise) {
		this.productMerchandise = productMerchandise;
	}

	@Column(name = "serviceFlag", nullable = true)
	public String getServiceFlag() {
		return serviceFlag;
	}

	public void setServiceFlag(String serviceFlag) {
		this.serviceFlag = serviceFlag;
	}

	@Column(name = "serviceType", nullable = true)
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	@Column(name = "systemChannel", nullable = true)
	public String getSystemChannel() {
		return systemChannel;
	}

	public void setSystemChannel(String systemChannel) {
		this.systemChannel = systemChannel;
	}

	@Column(name = "zip", nullable = true)
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Column(name = "iru", nullable = true)
	public String getIru() {
		return iru;
	}

	public void setIru(String iru) {
		this.iru = iru;
	}

	@Column(name = "fru", nullable = true)
	public String getFru() {
		return fru;
	}

	public void setFru(String fru) {
		this.fru = fru;
	}

	@Column(name = "unit", nullable = true)
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("ruleName", ruleName);
		builder.append("ruleDescription", ruleDescription);
		builder.append("startDate", startDate);
		builder.append("endDate", endDate);
		builder.append("paCoverage", paCoverage);
		builder.append("ssaCoverage", ssaCoverage);
		builder.append("minDaysPurchase", minDaysPurchase);
		builder.append("maxDaysPurchase", maxDaysPurchase);
		builder.append("minDaysInstall", minDaysInstall);
		builder.append("maxDaysInstall", maxDaysInstall);
		builder.append("minAttemptNumber", minAttemptNumber);
		builder.append("maxAttemptNumber", maxAttemptNumber);
		builder.append("minDaysW2", minDaysW2);
		builder.append("w2Offset", w2Offset);
		builder.append("brands", brands);
		builder.append("clients", clients);
		builder.append("creatorsUnit", creatorsUnit);
		builder.append("creatordId", creatordId);
		builder.append("coverage", coverage);
		builder.append("helpers", helpers);
		builder.append("partStatus", partStatus);
		builder.append("processId", processId);
		builder.append("products", products);
		builder.append("productMerchandise", productMerchandise);
		builder.append("serviceFlag", serviceFlag);
		builder.append("serviceType", serviceType);
		builder.append("systemChannel", systemChannel);
		builder.append("partStatus", partStatus);
		builder.append("zip", zip);
		builder.append("iru", iru);
		builder.append("fru", fru);
		builder.append("unit", unit);
		return builder.toString();
	}

	public boolean checkEmpty() {
		return (this.ruleName == null && this.ruleDescription == null && this.startDate == null && this.endDate == null
				&& this.paCoverage == null && this.ssaCoverage == null && this.minDaysPurchase == null
				&& this.maxDaysPurchase == null && this.minAttemptNumber == null && this.minDaysInstall == null
				&& this.maxAttemptNumber == null && this.minDaysW2 == null && this.w2Offset == null
				&& this.brands == null && this.clients == null && this.creatorsUnit == null && this.creatordId == null
				&& this.coverage == null && this.helpers == null && this.partStatus == null && this.processId == null
				&& this.products == null && this.productMerchandise == null && this.serviceFlag == null
				&& this.serviceType == null && this.systemChannel == null && this.partStatus == null && this.zip == null
				&& this.iru == null && this.fru == null && this.unit == null);
	}

}
