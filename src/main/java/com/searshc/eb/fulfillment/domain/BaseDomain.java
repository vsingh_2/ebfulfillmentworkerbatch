package com.searshc.eb.fulfillment.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

@MappedSuperclass
public class BaseDomain implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5047237339691783320L;
	private String createdBy;
	private Date createdTs;
	private String updatedBy;
	private Date updatedTs;

	@Column(name = "created_by", nullable = false, length = 255)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_ts", nullable = false, length = 10)
	public Date getCreatedTs() {
		return createdTs;
	}

	public void setCreatedTs(Date createdTs) {
		this.createdTs = createdTs;
	}

	@Column(name = "updated_by", nullable = true, length = 255)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_ts", nullable = true, length = 10)
	public Date getUpdatedTs() {
		return updatedTs;
	}

	public void setUpdatedTs(Date updatedTs) {
		this.updatedTs = updatedTs;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("createdBy", createdBy);
		builder.append("createdTs", createdTs);
		builder.append("updatedBy", updatedBy);
		builder.append("updatedTs", updatedTs);
		return builder.toString();
	}
}
