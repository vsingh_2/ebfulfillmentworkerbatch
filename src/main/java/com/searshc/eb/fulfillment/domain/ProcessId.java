package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.ProcessIdLookup;

@Entity
@Table(name = "eb_fulfillment_process_id")
public class ProcessId extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4002918556710721275L;
	private String processId;
	private Rule rule;
	private ProcessIdLookup processIdLookup;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_process_id", nullable = false, unique = true)
	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	// Foreign Key
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	// Foreign Key
	@OneToOne
	@JoinColumn(name = "process_id", nullable = false)
	public ProcessIdLookup getProcessIdLookup() {
		return processIdLookup;
	}

	public void setProcessIdLookup(ProcessIdLookup processIdLookup) {
		this.processIdLookup = processIdLookup;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("processId", processId);
		builder.append("rule", rule);
		builder.append("processIdLookup", processIdLookup);
		return builder.toString();
	}
}
