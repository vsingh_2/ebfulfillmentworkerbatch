package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.CoverageLookup;

@Entity
@Table(name = "eb_fulfillment_coverage")
public class Coverage extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 895654044020303825L;
	private String coverageId;
	private Rule rule;
	private CoverageLookup coverageLookup;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_coverage_id", nullable = false, unique = true)
	public String getCoverageId() {
		return coverageId;
	}

	public void setCoverageId(String coverageId) {
		this.coverageId = coverageId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "coverage_id", nullable = false)
	public CoverageLookup getCoverageLookup() {
		return coverageLookup;
	}

	public void setCoverageLookup(CoverageLookup coverageLookup) {
		this.coverageLookup = coverageLookup;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("coverageId", coverageId);
		builder.append("rule", rule);
		builder.append("coverageLookup", coverageLookup);
		return builder.toString();
	}
}
