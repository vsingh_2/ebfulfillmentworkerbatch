package com.searshc.eb.fulfillment.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.searshc.eb.fulfillment.lookup.domain.HelperTypeLookup;

@Entity
@Table(name = "eb_fulfillment_helper")
public class Helper extends BaseDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5064666008202033314L;
	private String helperId;
	private Rule rule;
	private HelperTypeLookup helperType;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ff_helper_id", nullable = false, unique = true)
	public String getHelperId() {
		return helperId;
	}

	public void setHelperId(String helperId) {
		this.helperId = helperId;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fulfillment_rule_id")
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@OneToOne
	@JoinColumn(name = "helper_type_id", nullable = false)
	public HelperTypeLookup getHelperType() {
		return helperType;
	}

	public void setHelperType(HelperTypeLookup helperType) {
		this.helperType = helperType;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
		builder.append("helperId", helperId);
		builder.append("rule", rule);
		builder.append("helperType", helperType);
		return builder.toString();
	}
}
