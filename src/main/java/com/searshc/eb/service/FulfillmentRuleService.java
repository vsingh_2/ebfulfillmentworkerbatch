package com.searshc.eb.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.searshc.eb.constant.Constants;
import com.searshc.eb.dao.FulfillmentRuleDAO;
import com.searshc.eb.fulfillment.domain.Audit;
import com.searshc.eb.fulfillment.domain.BaseDomain;
import com.searshc.eb.fulfillment.domain.EventBrokerLock;
import com.searshc.eb.fulfillment.domain.FileData;
import com.searshc.eb.fulfillment.domain.Rule;
import com.searshc.eb.fulfillment.domain.RulePriority;
import com.searshc.eb.fulfillment.domain.RuleUploadWorkQueue;

@Component
public class FulfillmentRuleService {

	@Autowired
	FulfillmentRuleDAO ruleDao;

	private static final Logger LOGGER = LoggerFactory.getLogger(FulfillmentRuleService.class);

	private RuleUploadWorkQueue workQueue = new RuleUploadWorkQueue();

	public void setAuditFields(BaseDomain domain) {
		domain.setCreatedBy(Constants.EBADMIN_USER);
		domain.setCreatedTs(new Timestamp(System.currentTimeMillis()));
		domain.setUpdatedBy(domain.getCreatedBy());
		domain.setUpdatedTs(domain.getCreatedTs());
	}

	public int deleteRules(boolean uploadFlag) {
		return ruleDao.deleteRules(uploadFlag);
	}	

	public boolean moveRules() {
		boolean uploadFlag = false;
		List<Rule> ruleList = ruleDao.getAllRules(uploadFlag);
		LOGGER.info("RuleList {} ", ruleList);
		// need to sort data based on priority
		int count = 1;
		for (Rule r : ruleList) {
			int priorityNum = ruleDao.findMaxPriority();
			RulePriority rulePriority = ruleDao.findRulePriorityByRule(r.getRuleId());
			RulePriority newRulePriority = rulePriority;
			int priority = priorityNum > 0 ? priorityNum : 1;
			newRulePriority.setPriority(priority);
			if (count == 1) {
				LOGGER.info(" old: {} ", rulePriority);
				LOGGER.info(" new: {} ", newRulePriority);
			}
			count = 2;
			// need to add auditing logic currently just updating value without
			// auditing it
			ruleDao.updateRulePriority(newRulePriority);
		}
		return true;
	}

	public String reorderRules() {
		LOGGER.info("Reordering the rules priority");
		String rearrangeString = "";
		List<RulePriority> rulePriorityList = ruleDao.getAllRulePriority();
		// rulePriorityList.sort(c);
		int newPriority = 1;
		for (RulePriority r : rulePriorityList) {
			String ruleValue = "rule-" + r.getRule().getRuleId();
			if (newPriority == 1) {
				rearrangeString = rearrangeString + ruleValue;
			} else {
				rearrangeString = rearrangeString + "," + ruleValue;
			}
			newPriority = newPriority + 1;
		}
		return rearrangeString;
	}

	public boolean arrangeRulesByPriority(String rulesOrderParam) {

		boolean success = true;
		try {
			List<RulePriority> existingPriorities = ruleDao.getAllRulePriority();
			for (RulePriority ep : existingPriorities) {
				// why we are calling this ?
				// Rule rule = ruleDao.getRuleById(ep.getRule().getRuleId());
				// rule.setPriority(null);
				Audit audit = new Audit();
				// need to add constants
				String operation = "Delete: " + ep.getClass();
				audit.setOperation(operation);
				audit.setNewValue(null);
				// need to write logic to auditValue for particular input
				audit.setOldValue(auditValue(ep));
				audit.setUpdatedBy(Constants.EBADMIN_USER);
				audit.setUpdatedTs(new Date());
				ruleDao.save(audit);
				int result = ruleDao.deleteRulePriorityById(ep.getRulePriorityId());
				if (result > 0) {
					LOGGER.info("Rule Priority Deleted successfully2");
				}
			}

			String[] rulesOrderArray = rulesOrderParam.split(",");
			for (int i = 0; i < rulesOrderArray.length; i++) {
				String ruleOrderStr = rulesOrderArray[i];
				int pos = ruleOrderStr.indexOf('-') + 1;
				String ruleId = ruleOrderStr.substring(pos);
				Rule rule = ruleDao.getRuleById(ruleId);
				if (rule != null) {
					RulePriority rulePriority = new RulePriority();
					rulePriority.setRule(rule);
					rulePriority.setPriority(i + 1);
					setAuditFields(rulePriority);
					ruleDao.save(rulePriority);
					Audit audit = new Audit();
					String operation = "Insert : " + rulePriority.getClass().getName();
					audit.setOperation(operation);
					audit.setOldValue(null);
					// need to write logic to auditValue for particular input
					audit.setNewValue(auditValue(rulePriority));
					audit.setUpdatedBy(Constants.EBADMIN_USER);
					audit.setUpdatedTs(new Date());
					ruleDao.save(audit);
				}
			}
		} catch (Exception e) {
			success = false;
			LOGGER.error("Exception occuered while arranging rule priority - {}", e.getMessage());
		}
		return success;
	}

	public String auditValue(BaseDomain domain) {
		StringBuilder sb = new StringBuilder();
		sb.append(domain.getClass().getName() + domain);
		return sb.toString();
	}

	public String findFileToProcess() {
		RuleUploadWorkQueue workQueue = ruleDao.getFileToProcess();
		if (workQueue != null) {
			LOGGER.info("Processing file name "+workQueue.getUserFileName());
			setFileBeingProcess(workQueue);
			return workQueue.getSystemFilePath();
		} else {
			LOGGER.info("No file left to process");
			return null;
		}
	}

	public void setFileBeingProcess(RuleUploadWorkQueue workQueue) {
		this.workQueue = workQueue;
	}

	public RuleUploadWorkQueue getFileBeingProcess() {
		return this.workQueue;
	}

	public int setFileStatusCode(char statusCode) {
		RuleUploadWorkQueue workQueue = getFileBeingProcess();
		System.out.println("work queue  " + workQueue);
		String workQueueId = workQueue.getRequestWorkqueueId();
		System.out.println("work queue id " + workQueueId);
		int result = ruleDao.setFileStatusCode(statusCode, workQueueId);
		return result;
	}

	public boolean checkFulfillmentLock() {
		return  ruleDao.checkFulfillmentLock();
	}
	
	public void aquireFulfillmentLock() {
		EventBrokerLock ebLock = new EventBrokerLock();
		ebLock.setBusinessFlow(Constants.BUSINESS_FLOW);
		ebLock.setLockStartTime(new Date());
		ebLock.setLockByUser(Constants.LOCK_USER);
		ebLock.setLockByUserType(Constants.USER_TYPE);
		setAuditFields(ebLock);
		ruleDao.aquireFulfillmentLock(ebLock);
	}
	
	public void releaseFulfillmentLock() {
		ruleDao.releaseFulfillmentLock();
	}

	public int deleteFileDataFromDB() {
		return ruleDao.deleteFileDataFromDB();
	}

	public List<FileData> getFileDataToProcess(int limit, int offset) {
		return ruleDao.getFileDataToProcess(limit, offset);
	}
}