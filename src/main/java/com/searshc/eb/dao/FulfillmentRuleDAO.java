package com.searshc.eb.dao;

import java.util.List;

import com.searshc.eb.fulfillment.domain.EventBrokerLock;
import com.searshc.eb.fulfillment.domain.FileData;
import com.searshc.eb.fulfillment.domain.Rule;
import com.searshc.eb.fulfillment.domain.RulePriority;
import com.searshc.eb.fulfillment.domain.RuleUploadWorkQueue;

//Main Logic still need to be written
public interface FulfillmentRuleDAO {


	public int findMaxPriority();

	public List<Rule> getAllRules(boolean uploadFlag);

	public List<RulePriority> getAllRulePriority();

	public RuleUploadWorkQueue getFileToProcess();

	public int deleteRules(boolean uploadFlag);

	public int setFileStatusCode(char statusCode, String workQueueId);

	public RulePriority findRulePriorityByRule(String ruleId);

	public void updateRulePriority(RulePriority newRulePriority);

	public Rule getRuleById(String ruleId);

	public void save(Object object);

	public int deleteRulePriorityById(String rulePriorityId);

	public boolean checkFulfillmentLock();
	
	public void aquireFulfillmentLock(EventBrokerLock ebLock);
	
	public boolean releaseFulfillmentLock();
	
	public int deleteFileDataFromDB();

	public List<FileData> getFileDataToProcess(int limit, int offset);
	
	public List<?> fetchAllData(Class<?> cls);
	
	public void saveRule(List<Rule> rules);
	
}
