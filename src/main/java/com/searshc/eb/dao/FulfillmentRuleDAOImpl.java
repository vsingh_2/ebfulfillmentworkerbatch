package com.searshc.eb.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.searshc.eb.constant.Constants;
import com.searshc.eb.fulfillment.domain.EventBrokerLock;
import com.searshc.eb.fulfillment.domain.FileData;
import com.searshc.eb.fulfillment.domain.Rule;
import com.searshc.eb.fulfillment.domain.RulePriority;
import com.searshc.eb.fulfillment.domain.RuleUploadWorkQueue;

@Repository("fulfillmentDAO")
@Transactional
public class FulfillmentRuleDAOImpl extends HibernateDaoSupport implements FulfillmentRuleDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(FulfillmentRuleDAOImpl.class);

	@Override
	public int findMaxPriority() {
		LOGGER.debug("Finding fulfillment rule max priority");
		try {
			String queryString = "select max(priority)+1 from RulePriority";
			Query query = currentSession().createQuery(queryString);
			return query.list().isEmpty() ? null : (Integer) query.list().get(0);
		} catch (RuntimeException re) {
			LOGGER.error("find max priority call failed");
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Rule> getAllRules(boolean uploadFlag) {
		LOGGER.debug("Finding All Rules instance with upload: {} and order by priority {} ", uploadFlag);
		try {
			String queryString = "from Rule r where r.uploadFlag=? order by r.priority";
			Query query = currentSession().createQuery(queryString);
			query.setParameter(0, uploadFlag);
			return query.list().isEmpty() ? null : query.list();
		} catch (RuntimeException re) {
			LOGGER.error("Finding All Rules call failed");
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RulePriority> getAllRulePriority() {
		LOGGER.debug("Finding All Rule Priority");
		try {
			String queryString = " from RulePriority r";
			Query query = currentSession().createQuery(queryString);
			return query.list().isEmpty() ? null : query.list();
		} catch (RuntimeException re) {
			LOGGER.error("Finding All Rule Prioritycall failed");
			throw re;
		}
	}

	@Override
	public RuleUploadWorkQueue getFileToProcess() {
		LOGGER.debug("Finding file that need to be process with statusCode = 'N' or 'R' ");
		try {
			String queryString = " from  RuleUploadWorkQueue r where r.statusCode = 'N' or r.statusCode = 'R' ";
			Query query = currentSession().createQuery(queryString);
			return query.list().isEmpty() ? null : (RuleUploadWorkQueue) query.list().get(0);
		} catch (RuntimeException re) {
			LOGGER.error("Finding file that need to be process call failed");
			throw re;
		}
	}

	@Override
	public int deleteRules(boolean uploadFlag) {
		LOGGER.debug("Deleting all rules with uploadFlag: {} ", uploadFlag);
		int result = 1;
		try {
			String queryString = "from Rule r where r.uploadFlag = :uploadFlag";
			Query query = currentSession().createQuery(queryString);
			query.setParameter("uploadFlag", uploadFlag);
			@SuppressWarnings("unchecked")
			List<Rule> ruleList = query.list().isEmpty() ? null : query.list();
			for (Rule r : ruleList) {
				currentSession().delete(r);
				/*currentSession().createQuery("delete from Brand b where b.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from Channel c where c.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from Client c where c.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from Coverage c where c.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from Creator c where c.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from Geography g where g.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from Helper h where h.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from PartStatus p where p.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from ProcessId p where p.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from Product p where p.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from ServiceFlags s where s.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from ServiceType s where s.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from RulePriority r where r.rule.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createQuery("delete from Rule r where r.ruleId=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();*/
				/*currentSession().createSQLQuery("delete from eb_fulfillment_brand where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_channel where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_client where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_coverage where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_creator where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_geo where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_helper where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_part_status where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_process_id where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_product where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_svc_flags where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_svc_type where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_priority where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();
				currentSession().createSQLQuery("delete from eb_fulfillment_rule where fulfillment_rule_id=:ruleId").setParameter("ruleId", r.getRuleId()).executeUpdate();*/
				/*currentSession().flush();
				currentSession().clear();*/
			}
		
		} catch (RuntimeException re) {
			result = 0;
			LOGGER.error("Deleting all rules call failed");
		}
		
		return result;
	}

	@Override
	public int setFileStatusCode(char statusCode, String workQueueId) {
		LOGGER.debug("Changing status code of RuleUploadWorkQueue instance with requestWorkqueueId: {} to {}",
				workQueueId, statusCode);
		try {
			String queryString = "update RuleUploadWorkQueue r set r.statusCode = ? where r.requestWorkqueueId = ?";
			Query query = currentSession().createQuery(queryString);
			query.setParameter(0, statusCode);
			query.setParameter(1, workQueueId);
			int result = query.executeUpdate();
			return result;
		} catch (RuntimeException re) {
			LOGGER.error("Changing status code of RuleUploadWorkQueue call failed");
			throw re;
		}
	}

	@Override
	public RulePriority findRulePriorityByRule(String ruleId) {
		LOGGER.debug("Finding RulePriority instance by ruleId: {} ", ruleId);
		try {
			String queryString = "from RulePriority r where r.rule.ruleId = ?";
			Query query = currentSession().createQuery(queryString);
			query.setParameter(0, ruleId);
			return query.list().isEmpty() ? null : (RulePriority) query.list().get(0);
		} catch (RuntimeException re) {
			LOGGER.error("Finding RulePriority call failed");
			throw re;
		}
	}

	@Override
	public void updateRulePriority(RulePriority newRulePriority) {
		LOGGER.debug("Updating Rule Priority {} instance ", newRulePriority);
		try {
			currentSession().saveOrUpdate(newRulePriority);
		} catch (RuntimeException re) {
			LOGGER.error("Updating Rule Priority call failed");
			throw re;
		}
	}

	@Override
	public Rule getRuleById(String ruleId) {
		LOGGER.debug("Finding Rule instance with ruleId: {} ", ruleId);
		try {
			String queryString = "from Rule r where r.ruleId = ?";
			Query query = currentSession().createQuery(queryString);
			query.setParameter(0, ruleId);
			return query.list().isEmpty() ? null : (Rule) query.list().get(0);
		} catch (RuntimeException re) {
			LOGGER.error("Finding Rule By Id call failed");
			throw re;
		}
	}

	@Override
	public void save(Object object) {
		LOGGER.debug("Saving Object {} instance", object.getClass().getName().toUpperCase());
		try {
			currentSession().save(object);
			/*currentSession().flush();*/
		} catch (RuntimeException re) {
			LOGGER.error("Saving Object call failed");
			throw re;
		}

	}
	
	@Override
	public void saveRule(List<Rule> rules) {
		//LOGGER.debug("Saving Object {} instance", object.getClass().getName().toUpperCase());
		try {
			int batchSize=0;
			for (Rule rule : rules) {
				currentSession().save(rule);
				batchSize++;
				if((batchSize%50==0) || (batchSize==rules.size())){
					currentSession().flush();
					currentSession().clear();
				}
			}
			
		} catch (RuntimeException re) {
			LOGGER.error("Saving Object call failed");
			throw re;
		}

	}
	

	@Override
	public int deleteRulePriorityById(String rulePriorityId) {
		LOGGER.debug("Deleting RulePriority instance with rulePriorityId: {} ", rulePriorityId);
		try {
			String queryString = "delete from RulePriority r where r.rulePriorityId = ?";
			Query query = currentSession().createQuery(queryString);
			query.setParameter(0, rulePriorityId);
			int result = query.executeUpdate();
			currentSession().flush();
			return result;
		} catch (RuntimeException re) {
			LOGGER.error("Deleting RulePriority call failed");
			throw re;
		}
	}

	@Override
	public boolean checkFulfillmentLock() {
		LOGGER.debug("Checking any user accuqired the global lock.");
		try {
			String queryString = "from EventBrokerLock e where e.lockEndTime = null and e.businessFlow=:businessFlow";
			Query query = currentSession().createQuery(queryString);
			query.setParameter("businessFlow", Constants.BUSINESS_FLOW);
			@SuppressWarnings("unchecked")
			List<EventBrokerLock> lockList = query.list();
			if (!lockList.isEmpty()) {
				for(EventBrokerLock eventBrokerLock : lockList){
					if (eventBrokerLock.getFulfillmentRuleId() == null && eventBrokerLock.getCapacityAreaPriorityId()== null) {						
						LOGGER.info("Already global lock aquired by user {} and type {}", eventBrokerLock.getLockByUser(), eventBrokerLock.getLockByUserType());
					}else{
						LOGGER.info("Already selected id {} lock aquired by user {} and type {}", eventBrokerLock.getEventBrokerLockId(), eventBrokerLock.getLockByUser(), eventBrokerLock.getLockByUserType());
					}
				}
				return true;
			}
			return false;
		} catch (RuntimeException re) {
			LOGGER.error("Checking any user accuqired the global lock call failed.");
			throw re;
		}
	}

	
	@Override
	public void aquireFulfillmentLock(EventBrokerLock ebLock) {
		LOGGER.debug("Aquiring the global lock for user type {}", ebLock.getLockByUser());
		try {
			Session session = currentSession();
			session.save(ebLock);
			session.flush();
		} catch (RuntimeException re) {
			LOGGER.error("Aquiring the global lock for user type {} failed.", ebLock.getLockByUser());
			throw re;
		}
	}
	
	
	@Override
	public boolean releaseFulfillmentLock() {
		LOGGER.debug("Releasing the aquired global lock.");
		try {
			String queryString = "update EventBrokerLock e set e.lockEndTime=:lockEndTime, e.updatedTs=:updatedTs, e.updatedBy=:updatedBy where e.lockEndTime = null and e.businessFlow=:businessFlow and e.fulfillmentRuleId=null and e.capacityAreaPriorityId=null";
			Query query = currentSession().createQuery(queryString);
			query.setParameter("lockEndTime", new Date());
			query.setParameter("updatedTs", new Date());
			query.setParameter("updatedBy", Constants.LOCK_USER);
			query.setParameter("businessFlow", Constants.BUSINESS_FLOW);
			int count = query.executeUpdate();
			return count>0 ? true :false;
		} catch (RuntimeException re) {
			LOGGER.error("Releasing the aquired global lock failed.");
			throw re;
		}
	}
	
	
	@Override
	@Transactional
	public int deleteFileDataFromDB() {
		LOGGER.debug("Deleting previous uploaded file data from database");
		int result;
		try {
			String queryString = "delete from FileData f";	
			Query query = currentSession().createQuery(queryString);
			result = query.executeUpdate();
			currentSession().flush();
		} catch (RuntimeException re) {
			result = 0;
			LOGGER.error("delete file data from DB call failed");
			throw re;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FileData> getFileDataToProcess(int limit, int offset) {
		LOGGER.debug("Getting FileData instance from database upto offset {}", offset);
		try {
			String queryString = "from FileData f";
			Query query = currentSession().createQuery(queryString);
			query.setFirstResult(offset); // similar to offset
			query.setMaxResults(limit); // similar to limit
			return query.list().isEmpty() ? null : query.list();
		} catch (RuntimeException re) {
			LOGGER.error("Get file data from DB call failed");
			throw re;
		}
	}
		
	public  List<?>  fetchAllData(Class<?> cls) {
		LOGGER.debug("Loading all data for {}", cls);
		try {
			//String queryString = "from HelperTypeLookup";
			List<?> list = currentSession().createCriteria(cls).list();
			return list.isEmpty() ? null : list;
		} catch (RuntimeException re) {
			LOGGER.error("Loading the all {} data failed", cls);
			throw re;
		}
	}
	
}