package com.searshc.eb.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;

public class StepLoggerListener {

	private static final Logger lOGGER = LoggerFactory.getLogger(StepLoggerListener.class);

	@BeforeStep
	public void beforeStep(StepExecution exec) {
		lOGGER.info("Starting step : " + exec.getStepName());
	}

	@AfterStep
	public ExitStatus afterStep(StepExecution exec) {
		lOGGER.info("Total number of records read is  :" + exec.getReadCount());
		lOGGER.info("Total number of records skipped during read: " + exec.getReadSkipCount());
		lOGGER.info("Total number of records skipped during process: " + exec.getProcessSkipCount());
		lOGGER.info("Total number of records skipped during write: " + exec.getWriteSkipCount());
		lOGGER.info("Total number of records written is :" + exec.getWriteCount());
		lOGGER.info("Finished step " + exec.getStepName());
		return exec.getExitStatus();
	}
}
