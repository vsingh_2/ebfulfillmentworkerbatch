package com.searshc.eb.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeJob;
import org.springframework.beans.factory.annotation.Autowired;

import com.searshc.eb.service.FulfillmentRuleService;
import com.searshc.eb.ses.service.AmazonSESService;
import com.searshc.eb.ses.service.EmailTemplateData;

public class FulfillmentRuleJobListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(FulfillmentRuleJobListener.class);

	private long startTime = 0l;

	@Autowired
	FulfillmentRuleService fulfillmentService;

	@Autowired
	AmazonSESService emailService;
	
	@BeforeJob
	public void beforeJob(JobExecution exec) {
		startTime = System.nanoTime();
		LOGGER.info("Starting Job :" + exec.getJobInstance().getJobName());
	}

	@AfterJob
	public void afterJob(JobExecution exec) throws Exception {
		long endTime = System.nanoTime();
		int result;
		if (exec.getStatus() == BatchStatus.COMPLETED) {
			result = setFileStatus('C');
			fulfillmentService.releaseFulfillmentLock();
			sendEmail(exec);
			if (result > 1) {
				LOGGER.info("work queue file status updated successfully");
			}
			LOGGER.info("Finished job " + exec.getJobInstance().getJobName() + " successfully in "
					+ (endTime - startTime) / 1E9 + " seconds.");
		} else if (exec.getStatus() == BatchStatus.FAILED) {
			result = setFileStatus('R');
			fulfillmentService.releaseFulfillmentLock();
			sendEmail(exec);
			if (result > 1) {
				LOGGER.info("work queue file status updated successfully");
			}
			LOGGER.info("Finished job " + exec.getJobInstance().getJobName() + " in error in "
					+ (endTime - startTime) / 1E9 + " seconds.");
		} else {
			result = setFileStatus('R');
			fulfillmentService.releaseFulfillmentLock();
			if (result > 1) {
				LOGGER.info("work queue file status updated successfully");
			}
			LOGGER.info("Finished job " + exec.getJobInstance().getJobName() + " with " + exec.getStatus()
					+ " status in " + (endTime - startTime) / 1E9 + " seconds.");
		}
	}

	private int setFileStatus(char statusCode){
		int result = 0;
		if (fulfillmentService.getFileBeingProcess().getUserFileName() != null) {
			result = fulfillmentService.setFileStatusCode(statusCode);
		}
		return result;
	}
	private void sendEmail(JobExecution exec){
		try {
			if (fulfillmentService.getFileBeingProcess().getUserFileName() != null) {
				EmailTemplateData templateData = new EmailTemplateData();
				templateData.setExceptionMsg("");
				templateData.setStatus(exec.getStatus().name());
				templateData.setTeam("HSRD Team");
				templateData.setName("PSOM");
				templateData.setUserFileName(fulfillmentService.getFileBeingProcess().getUserFileName());
				emailService.sendEmail(templateData);
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while sending email - {}", e.getMessage());
		}
	}
}
