package com.searshc.eb.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.annotation.OnSkipInProcess;
import org.springframework.batch.core.annotation.OnSkipInRead;
import org.springframework.batch.core.annotation.OnSkipInWrite;

import com.searshc.eb.fulfillment.domain.FileData;
import com.searshc.eb.fulfillment.domain.Rule;

public class FulfillmentRuleJobSkipListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(FulfillmentRuleJobSkipListener.class);

	@OnSkipInWrite
	void onSkipInWrite(Rule rule, Throwable throwable) {
		LOGGER.debug("Entering onSkipInWrite");
		LOGGER.error("Record {} is skipped.", rule);
		LOGGER.error(throwable.getMessage(), throwable);
		LOGGER.debug("Exiting onSkipInWrite");
	}

	@OnSkipInRead
	void onSkipInRead(Throwable throwable) {
		LOGGER.debug("Entering onSkipInRead");
		LOGGER.error(throwable.getMessage(), throwable);
		LOGGER.debug("Exiting onSkipInRead");
	}

	@OnSkipInProcess
	void onSkipInProcess(FileData fileData, Throwable throwable) {
		LOGGER.debug("Entering onSkipInProcess");
		LOGGER.error("Error encountered with record {} in processor.", fileData);
		LOGGER.error(throwable.getMessage(), throwable);
		LOGGER.debug("Exiting onSkipInProcess");
	}

}
