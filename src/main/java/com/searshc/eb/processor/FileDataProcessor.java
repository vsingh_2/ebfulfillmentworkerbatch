package com.searshc.eb.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import com.searshc.eb.fulfillment.domain.FileData;
import com.searshc.eb.service.FulfillmentRuleService;

public class FileDataProcessor implements ItemProcessor<FileData, FileData> {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileDataProcessor.class);

	@Autowired
	FulfillmentRuleService ruleService;

	@Override
	public FileData process(FileData data) throws Exception {
		LOGGER.info("Processing data {}", data);
		if (!data.checkEmpty()) {
			ruleService.setAuditFields(data);
		}
		return data;
	}
}