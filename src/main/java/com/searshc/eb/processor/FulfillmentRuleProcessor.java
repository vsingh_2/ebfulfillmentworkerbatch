package com.searshc.eb.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.searshc.eb.constant.Constants;
import com.searshc.eb.dao.FulfillmentRuleDAO;
import com.searshc.eb.fulfillment.domain.Brand;
import com.searshc.eb.fulfillment.domain.Channel;
import com.searshc.eb.fulfillment.domain.Client;
import com.searshc.eb.fulfillment.domain.Coverage;
import com.searshc.eb.fulfillment.domain.Creator;
import com.searshc.eb.fulfillment.domain.FileData;
import com.searshc.eb.fulfillment.domain.Geography;
import com.searshc.eb.fulfillment.domain.Helper;
import com.searshc.eb.fulfillment.domain.PartStatus;
import com.searshc.eb.fulfillment.domain.ProcessId;
import com.searshc.eb.fulfillment.domain.Product;
import com.searshc.eb.fulfillment.domain.Rule;
import com.searshc.eb.fulfillment.domain.RulePriority;
import com.searshc.eb.fulfillment.domain.ServiceFlags;
import com.searshc.eb.fulfillment.domain.ServiceType;
import com.searshc.eb.fulfillment.lookup.domain.BrandLookup;
import com.searshc.eb.fulfillment.lookup.domain.ChannelLookup;
import com.searshc.eb.fulfillment.lookup.domain.ClientLookup;
import com.searshc.eb.fulfillment.lookup.domain.CoverageLookup;
import com.searshc.eb.fulfillment.lookup.domain.CreatorTypeLookup;
import com.searshc.eb.fulfillment.lookup.domain.GeographyTypeLookup;
import com.searshc.eb.fulfillment.lookup.domain.HelperTypeLookup;
import com.searshc.eb.fulfillment.lookup.domain.MerchandiseTypeLookup;
import com.searshc.eb.fulfillment.lookup.domain.PartStatusTypeLookup;
import com.searshc.eb.fulfillment.lookup.domain.ProcessIdLookup;
import com.searshc.eb.fulfillment.lookup.domain.ServiceFlagLookup;
import com.searshc.eb.fulfillment.lookup.domain.ServiceProviderLookup;
import com.searshc.eb.fulfillment.lookup.domain.ServiceTypeLookup;
import com.searshc.eb.service.FulfillmentRuleService;

@Component
public class FulfillmentRuleProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(FulfillmentRuleProcessor.class);
	
	@Autowired
	FulfillmentRuleService ruleService;

	@Autowired
	FulfillmentRuleDAO ruleDao;
		
	private CacheManager cacheManager;
	
	
	private void cacheAllFulfillmentData(){
		this.cacheManager = CacheManager.getInstance();
		buildCache(cacheManager);
		}


	private Object getObject(String cacheName, Object key) {
		Element element = cacheManager.getCache(cacheName).get(key);
		if (element != null) {
			System.out.println("Cache Name :"+cacheName+", Found key - "+key);
			return element.getObjectValue();
		}else{
			System.out.println("Cache Name :"+cacheName+", key - "+key+" not found.");
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private void buildCache(CacheManager cacheManager){
		 LOGGER.info("Lookup cache building start.");
		 
		 Cache cache = cacheManager.getCache(Constants.BRANDLOOKUP_CACHE);
		 List<BrandLookup> brands = (List<BrandLookup>) ruleDao.fetchAllData(BrandLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.BRANDLOOKUP_CACHE, brands);
		 for (BrandLookup brandLookup : brands) {
			 Element element =  new Element(brandLookup.getBrandName(), brandLookup);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.CHANNELLOOKUP_CACHE);
		 List<ChannelLookup> channels = (List<ChannelLookup>) ruleDao.fetchAllData(ChannelLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.CHANNELLOOKUP_CACHE, channels);
		 for (ChannelLookup channel : channels) {
			 Element element =  new Element(channel.getChannelDescription(), channel);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.CLIENTLOOKUP_CACHE);
		 List<ClientLookup> clients = (List<ClientLookup>) ruleDao.fetchAllData(ClientLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.CLIENTLOOKUP_CACHE, clients);
		 for (ClientLookup client : clients) {
			 Element element =  new Element(client.getClientCode(), client);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.COVERAGELOOKUP_CACHE);
		 List<CoverageLookup> coverages = (List<CoverageLookup>) ruleDao.fetchAllData(CoverageLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.COVERAGELOOKUP_CACHE, coverages);
		 for (CoverageLookup coverage : coverages) {
			 Element element =  new Element(coverage.getCoverageCode(), coverage);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.CREATORTYPELOOKUP_CACHE);
		 List<CreatorTypeLookup> creatorTypes = (List<CreatorTypeLookup>) ruleDao.fetchAllData(CreatorTypeLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.CREATORTYPELOOKUP_CACHE, creatorTypes);
		 for (CreatorTypeLookup creatorType : creatorTypes) {
			 Element element =  new Element(creatorType.getReqCreatorTypeDescription(), creatorType);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.GEOGRAPHYTYPELOOKUP_CACHE);
		 List<GeographyTypeLookup> geographyTypeLookups = (List<GeographyTypeLookup>) ruleDao.fetchAllData(GeographyTypeLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.GEOGRAPHYTYPELOOKUP_CACHE, geographyTypeLookups);
		 for (GeographyTypeLookup geographyTypeLookup : geographyTypeLookups) {
			 Element element =  new Element(geographyTypeLookup.getGeoTypeCode(), geographyTypeLookup);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.HELPERTYPELOOKUP_CACHE);
		 List<HelperTypeLookup> helperTypeLookups = (List<HelperTypeLookup>) ruleDao.fetchAllData(HelperTypeLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.HELPERTYPELOOKUP_CACHE, helperTypeLookups);
		 for (HelperTypeLookup helperTypeLookup : helperTypeLookups) {
			 Element element =  new Element(helperTypeLookup.getHelperTypeDescription(), helperTypeLookup);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.MERCHANDISETYPELOOKUP_CACHE);
		 List<MerchandiseTypeLookup> merchandiseTypeLookups = (List<MerchandiseTypeLookup>) ruleDao.fetchAllData(MerchandiseTypeLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.MERCHANDISETYPELOOKUP_CACHE, merchandiseTypeLookups);
		 for (MerchandiseTypeLookup merchandiseTypeLookup : merchandiseTypeLookups) {
			 Element element =  new Element(merchandiseTypeLookup.getMerchandiseTypeCode(), merchandiseTypeLookup);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.PARTSTATUSTYPELOOKUP_CACHE);
		 List<PartStatusTypeLookup> partStatusTypeLookups = (List<PartStatusTypeLookup>) ruleDao.fetchAllData(PartStatusTypeLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.PARTSTATUSTYPELOOKUP_CACHE, partStatusTypeLookups);
		 for (PartStatusTypeLookup partStatusTypeLookup : partStatusTypeLookups) {
			 Element element =  new Element(partStatusTypeLookup.getPartStatusDescription(), partStatusTypeLookup);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.PROCESSIDLOOKUP_CACHE);
		 List<ProcessIdLookup> processIdLookups = (List<ProcessIdLookup>) ruleDao.fetchAllData(ProcessIdLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.PROCESSIDLOOKUP_CACHE, processIdLookups);
		 for (ProcessIdLookup processIdLookup : processIdLookups) {
			 Element element =  new Element(processIdLookup.getProcessIdCode(), processIdLookup);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.SERVICEFLAGLOOKUP_CACHE);
		 List<ServiceFlagLookup> serviceFlagLookups = (List<ServiceFlagLookup>) ruleDao.fetchAllData(ServiceFlagLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.SERVICEFLAGLOOKUP_CACHE, serviceFlagLookups);
		 for (ServiceFlagLookup serviceFlagLookup : serviceFlagLookups) {
			 Element element =  new Element(serviceFlagLookup.getServiceFlagDescription(), serviceFlagLookup);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.SERVICEPROVIDERLOOKUP_CACHE);
		 List<ServiceProviderLookup> serviceProviderLookups = (List<ServiceProviderLookup>) ruleDao.fetchAllData(ServiceProviderLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.SERVICEPROVIDERLOOKUP_CACHE, serviceProviderLookups);
		 for (ServiceProviderLookup serviceProviderLookup : serviceProviderLookups) {
			 Element element =  new Element(serviceProviderLookup.getServicerName(), serviceProviderLookup);
			 cache.put(element);
		 }
		 
		 cache = cacheManager.getCache(Constants.SERVICETYPELOOKUP_CACHE);
		 List<ServiceTypeLookup> serviceTypeLookups = (List<ServiceTypeLookup>) ruleDao.fetchAllData(ServiceTypeLookup.class);
		 LOGGER.debug("All {} cahce data - {} ", Constants.SERVICETYPELOOKUP_CACHE, serviceTypeLookups);
		 for (ServiceTypeLookup serviceTypeLookup : serviceTypeLookups) {
			 Element element =  new Element(serviceTypeLookup.getServiceTypeDescription(), serviceTypeLookup);
			 cache.put(element);
		 }		
		 LOGGER.info("Lookup cache initialization is done.");
	 }
	public void processFulfillmentRule() throws Exception {
		cacheAllFulfillmentData();
		int offset = 0;
		int limit = 25;
		boolean process = true;
		while (process) {
			List<FileData> fileDataList = ruleService.getFileDataToProcess(limit, offset);
			if (fileDataList == null) {
				process = false;
			} else {
				//List<Rule> rules = new ArrayList<Rule>();
				for (FileData fileData : fileDataList) {
					Rule rule = populateFileDataToRule(fileData);
					ruleDao.save(rule);
				//	rules.add(rule);
				}
				//ruleDao.saveRule(rules);
				offset = offset + 25;
			}
		}
		clearAndShutDownCache();
	}

	public Rule populateFileDataToRule(FileData data) throws Exception {
		Rule rule = new Rule();
		String datePattern = "yyyy/MM/dd";
		SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
		if (data.getRuleDescription() != null && data.getRuleDescription() != null) {
			rule.setRuleName(data.getRuleName());
			rule.setRuleDescription(data.getRuleDescription());
		}
		if (data.getStartDate() != null && data.getStartDate() != "") {
			Date newStartDate = formatter.parse(data.getStartDate());
			rule.setStartDate(newStartDate);
		} else {
			rule.setStartDate(null);
		}
		if (data.getEndDate() != null && data.getEndDate() != "") {
			Date newEndDate = formatter.parse(data.getEndDate());
			rule.setEndDate(newEndDate);
		} else {
			rule.setEndDate(null);
		}
		if (data.getPaCoverage() != null && data.getPaCoverage() != "") {
			rule.setPaCoverageFlag(data.getPaCoverage());
		} else {
			rule.setPaCoverageFlag(null);
		}
		if (data.getSsaCoverage() != null && data.getSsaCoverage() != "") {
			rule.setSsaCoverageFlag(data.getSsaCoverage());
		} else {
			rule.setSsaCoverageFlag(null);
		}
		if (data.getMinDaysPurchase() != null && data.getMinDaysPurchase() != "") {
			rule.setMinDaysFromPurchase(Integer.parseInt(data.getMinDaysPurchase()));
		} else {
			rule.setMinDaysFromPurchase(null);
		}
		if (data.getMaxDaysPurchase() != null && data.getMaxDaysPurchase() != "") {
			rule.setMaxDaysFromPurchase(Integer.parseInt(data.getMaxDaysPurchase()));
		} else {
			rule.setMaxDaysFromPurchase(null);
		}
		if (data.getMinDaysInstall() != null && data.getMinDaysInstall() != "") {
			rule.setMinDaysFromInstallation(Integer.parseInt(data.getMinDaysInstall()));
		} else {
			rule.setMinDaysFromInstallation(null);
		}
		if (data.getMaxDaysInstall() != null && data.getMaxDaysInstall() != "") {
			rule.setMaxDaysFromInstallation(Integer.parseInt(data.getMaxDaysInstall()));
		} else {
			rule.setMaxDaysFromInstallation(null);
		}
		if (data.getMinAttemptNumber() != null && data.getMinAttemptNumber() != "") {
			rule.setMinAttemptNum(Integer.parseInt(data.getMinAttemptNumber()));
		} else {
			rule.setMinAttemptNum(null);
		}
		if (data.getMaxAttemptNumber() != null && data.getMaxAttemptNumber() != "") {
			rule.setMaxAttemptNum(Integer.parseInt(data.getMaxAttemptNumber()));
		} else {
			rule.setMaxAttemptNum(null);
		}
		if (data.getMinDaysW2() != null && data.getMinDaysW2() != "") {
			rule.setMinDaysToW2(Integer.parseInt(data.getMinDaysW2()));
		} else {
			rule.setMinDaysToW2(null);
		}
		if (data.getW2Offset() != null && data.getW2Offset() != "") {
			rule.setW2Offset(Integer.parseInt(data.getW2Offset()));
		} else {
			rule.setW2Offset(null);
		}
		rule.setUploadFlag(true);
		rule.setProtectedFlag(false);
		if (data.getServiceProvider() != null && data.getServiceProvider() != "") {
			ServiceProviderLookup provider = (ServiceProviderLookup) getObject(Constants.SERVICEPROVIDERLOOKUP_CACHE, data.getServiceProvider());//ruleDao.findByServicerName(data.getServiceProvider());
			rule.setProvider(provider);
		} else {
			rule.setProvider(null);
		}
		
		// Create Rule Priority
		RulePriority priority = new RulePriority();
		int priorityNum = ruleDao.findMaxPriority();
		priority.setPriority(priorityNum);
		// need to change this logic
		ruleService.setAuditFields(priority);
		priority.setRule(rule);
		rule.setPriority(priority);
		// Create client
		if (data.getClients() != null) {
			String[] clientList = data.getClients().split(Constants.SEPERATOR);
			List<Client> ruleClientList = new ArrayList<Client>();
			for (String c : clientList) {
				Client client = new Client();
				ClientLookup validClient = (ClientLookup) getObject(Constants.CLIENTLOOKUP_CACHE, c);
				if (validClient != null) {
					client.setClient(validClient);
					ruleService.setAuditFields(client);
					client.setRule(rule);
					ruleClientList.add(client);
				}
			}
			rule.setClients(ruleClientList);
		}
		// Create Brand
		if (data.getBrands() != null) {
			String[] brandList = data.getBrands().split(Constants.SEPERATOR);
			List<Brand> ruleBrandList = new ArrayList<Brand>();
			for (String b : brandList) {
				Brand brand = new Brand();
				BrandLookup validBrands = (BrandLookup) getObject(Constants.BRANDLOOKUP_CACHE, b);
				if ( validBrands!= null){
					brand.setBrandLookup(validBrands);
					ruleService.setAuditFields(brand);
					brand.setRule(rule);
					ruleBrandList.add(brand);
				}
			}
			rule.setBrands(ruleBrandList);
		}
		// Create Coverage
		if (data.getCoverage() != null) {
			String[] coverageList = data.getCoverage().split(Constants.SEPERATOR);
			List<Coverage> ruleCoverageList = new ArrayList<Coverage>();
			for (String c : coverageList) {
				Coverage coverage = new Coverage();
				CoverageLookup validCoverage = (CoverageLookup) getObject(Constants.COVERAGELOOKUP_CACHE, c);
				if (validCoverage!= null){//validCoverage.size() == 1) {
					coverage.setCoverageLookup(validCoverage);
					ruleService.setAuditFields(coverage);
					coverage.setRule(rule);
					ruleCoverageList.add(coverage);
				}
			}
			rule.setCoverages(ruleCoverageList);
		}
		// create creator unit
		List<Creator> ruleCreatorList = new ArrayList<Creator>();
		if (data.getCreatorsUnit() != null) {
			CreatorTypeLookup creatorType = (CreatorTypeLookup) getObject(Constants.CREATORTYPELOOKUP_CACHE, Constants.UNIT);
			if (creatorType != null && data.getCreatorsUnit() != null) {
				String[] creatorUnitList = data.getCreatorsUnit().split(Constants.SEPERATOR);
				for (String c : creatorUnitList) {
					Creator creator = new Creator();
					creator.setReqCreatorId(c);
					creator.setCreatorType(creatorType);
					ruleService.setAuditFields(creator);
					creator.setRule(rule);
					ruleCreatorList.add(creator);
				}
			}
			rule.setCreators(ruleCreatorList);
		}
		// create creator user
		if (data.getCreatordId() != null) {
			CreatorTypeLookup creatorType = (CreatorTypeLookup) getObject(Constants.CREATORTYPELOOKUP_CACHE, Constants.USER);
			if (creatorType != null && data.getCreatordId() != null) {
				String[] creatorIdList = data.getCreatordId().split(Constants.SEPERATOR);
				for (String c : creatorIdList) {
					Creator creator = new Creator();
					creator.setReqCreatorId(c);
					creator.setCreatorType(creatorType);
					ruleService.setAuditFields(creator);
					creator.setRule(rule);
					ruleCreatorList.add(creator);
				}
			}
			rule.setCreators(ruleCreatorList);
		}
		// Create Helper
		if (data.getHelpers() != null) {
			String[] helperList = data.getHelpers().split(Constants.SEPERATOR);
			List<Helper> ruleHelperList = new ArrayList<Helper>();
			for (String h : helperList) {
				Helper helper = new Helper();
				HelperTypeLookup validHelper = (HelperTypeLookup) getObject(Constants.HELPERTYPELOOKUP_CACHE, h);
				if (validHelper != null) {
					helper.setHelperType(validHelper);
					ruleService.setAuditFields(helper);
					helper.setRule(rule);
					ruleHelperList.add(helper);
				}
			}
			rule.setHelpers(ruleHelperList);
		}
		// Create PartStatus
		if (data.getPartStatus() != null) {
			String[] partStatusList = data.getPartStatus().split(Constants.SEPERATOR);
			List<PartStatus> rulePartStatusList = new ArrayList<PartStatus>();
			for (String p : partStatusList) {
				PartStatus partStatus = new PartStatus();
				PartStatusTypeLookup validPartStatus = (PartStatusTypeLookup) getObject(Constants.PARTSTATUSTYPELOOKUP_CACHE, p);
				if (validPartStatus != null) {
					partStatus.setPartLookup(validPartStatus);
					ruleService.setAuditFields(partStatus);
					partStatus.setRule(rule);
					rulePartStatusList.add(partStatus);
				}
			}
			rule.setPartStatuses(rulePartStatusList);
		}
		// create processid
		if (data.getProcessId() != null) {
			String[] processIdList = data.getProcessId().split(Constants.SEPERATOR);
			List<ProcessId> ruleProcessIdList = new ArrayList<ProcessId>();
			for (String p : processIdList) {
				ProcessId processId = new ProcessId();
				ProcessIdLookup validprocessId = (ProcessIdLookup) getObject(Constants.PROCESSIDLOOKUP_CACHE, p);
				if (validprocessId != null) {
					processId.setProcessIdLookup(validprocessId);
					ruleService.setAuditFields(processId);
					processId.setRule(rule);
					ruleProcessIdList.add(processId);
				}
			}
			rule.setProcs(ruleProcessIdList);
		}
		// create product spec
		List<Product> ruleProductList = new ArrayList<Product>();
		if (data.getProducts() != null) {
			MerchandiseTypeLookup merchandiseType = (MerchandiseTypeLookup) getObject(Constants.MERCHANDISETYPELOOKUP_CACHE, Constants.SPEC);
			if (merchandiseType != null && !data.getProducts().isEmpty()) {
				String[] prodSpecList = data.getProducts().split(Constants.SEPERATOR);
				for (String m : prodSpecList) {
					Product product = new Product();
					product.setMerchValue(m);
					product.setMerchType(merchandiseType);
					ruleService.setAuditFields(product);
					product.setRule(rule);
					ruleProductList.add(product);
				}
			}
			rule.setProducts(ruleProductList);
		}
		// create product merch
		if (data.getProductMerchandise() != null) {
			MerchandiseTypeLookup merchandiseTypeInst = (MerchandiseTypeLookup) getObject(Constants.MERCHANDISETYPELOOKUP_CACHE, Constants.MERCH);
			if (merchandiseTypeInst != null && data.getProductMerchandise() != null) {
				String[] prodMerchList = data.getProductMerchandise().split(Constants.SEPERATOR);
				for (String m : prodMerchList) {
					Product product = new Product();
					product.setMerchValue(m);
					product.setMerchType(merchandiseTypeInst);
					ruleService.setAuditFields(product);
					product.setRule(rule);
					ruleProductList.add(product);
				}
			}
			rule.setProducts(ruleProductList);
		}
		// Create ServiceFlags
		if (data.getServiceFlag() != null) {
			String[] serviceFlagsList = data.getServiceFlag().split(Constants.SEPERATOR);
			List<ServiceFlags> ruleServiceFlagsList = new ArrayList<ServiceFlags>();
			for (String s : serviceFlagsList) {
				ServiceFlags serviceFlags = new ServiceFlags();
				ServiceFlagLookup validServiceFlag = (ServiceFlagLookup) getObject(Constants.SERVICEFLAGLOOKUP_CACHE, s);
				if (validServiceFlag!= null){
					serviceFlags.setServiceFlagLookup(validServiceFlag);
					ruleService.setAuditFields(serviceFlags);
					serviceFlags.setRule(rule);
					ruleServiceFlagsList.add(serviceFlags);
				}
			}
			rule.setServiceFlags(ruleServiceFlagsList);
		}
		// Create ServiceType
		if (data.getServiceType() != null) {
			String[] serviceTypeList = data.getServiceType().split(Constants.SEPERATOR);
			List<ServiceType> ruleServiceTypeList = new ArrayList<ServiceType>();
			for (String s : serviceTypeList) {
				ServiceType serviceType = new ServiceType();
				ServiceTypeLookup validServiceType = (ServiceTypeLookup) getObject(Constants.SERVICETYPELOOKUP_CACHE, s);
				if (validServiceType!= null){
					serviceType.setServiceTypeLookup(validServiceType);
					ruleService.setAuditFields(serviceType);
					serviceType.setRule(rule);
					ruleServiceTypeList.add(serviceType);
				}
			}
			rule.setServiceTypes(ruleServiceTypeList);
		}
		// Create Channel
		if (data.getSystemChannel() != null) {
			String[] channelList = data.getSystemChannel().split(Constants.SEPERATOR);
			List<Channel> ruleChannelList = new ArrayList<Channel>();
			for (String c : channelList) {
				Channel channel = new Channel();
				ChannelLookup validChannel = (ChannelLookup) getObject(Constants.CHANNELLOOKUP_CACHE, c);
				if (validChannel!= null){
					channel.setChannelLookup(validChannel);
					ruleService.setAuditFields(channel);
					channel.setRule(rule);
					ruleChannelList.add(channel);
				}
			}
			rule.setChannels(ruleChannelList);
		}
		// Create Zip
		List<Geography> ruleGeographyList = new ArrayList<Geography>();
		if (data.getZip() != null) {
			GeographyTypeLookup zipType = (GeographyTypeLookup) getObject(Constants.GEOGRAPHYTYPELOOKUP_CACHE, Constants.ZIP);
			String[] zips = data.getZip().split(Constants.SEPERATOR);
			for (String z : zips) {
				Geography geography = new Geography();
				geography.setGeoValue(z.trim());
				geography.setGeoType(zipType);
				ruleService.setAuditFields(geography);
				geography.setRule(rule);
				ruleGeographyList.add(geography);
			}
			rule.setGeos(ruleGeographyList);
		}
		if (data.getIru() != null) {
			GeographyTypeLookup iruType = (GeographyTypeLookup) getObject(Constants.GEOGRAPHYTYPELOOKUP_CACHE, Constants.IRU);
			String[] irus = data.getIru().split(Constants.SEPERATOR);
			for (String i : irus) {
				Geography geography = new Geography();
				geography.setGeoValue(i.trim());
				geography.setGeoType(iruType);
				ruleService.setAuditFields(geography);
				geography.setRule(rule);
				ruleGeographyList.add(geography);
			}
			rule.setGeos(ruleGeographyList);
		}
		if (data.getUnit() != null) {
			GeographyTypeLookup unitType = (GeographyTypeLookup) getObject(Constants.GEOGRAPHYTYPELOOKUP_CACHE, Constants.UNIT);
			String[] units = data.getUnit().split(Constants.SEPERATOR);
			for (String u : units) {
				Geography geography = new Geography();
				geography.setGeoValue(u.trim());
				geography.setGeoType(unitType);
				ruleService.setAuditFields(geography);
				geography.setRule(rule);
				ruleGeographyList.add(geography);
			}
			rule.setGeos(ruleGeographyList);
		}

		if (data.getFru() != null) {
			GeographyTypeLookup fruType = (GeographyTypeLookup) getObject(Constants.GEOGRAPHYTYPELOOKUP_CACHE, Constants.FRU);
			String[] frus = data.getFru().split(Constants.SEPERATOR);
			for (String f : frus) {
				Geography geography = new Geography();
				geography.setGeoValue(f.trim());
				geography.setGeoType(fruType);
				ruleService.setAuditFields(geography);
				geography.setRule(rule);
				ruleGeographyList.add(geography);
			}
			rule.setGeos(ruleGeographyList);
		}
		ruleService.setAuditFields(rule);
		return rule;
	}
	
	private void clearAndShutDownCache() {
		LOGGER.info("Removing the cache start as processing part done.");
		try {
			cacheManager.removeAllCaches();
			cacheManager.shutdown();
		} catch (Exception e) {
			LOGGER.error("Failed to clear and shut down");
		}
		LOGGER.info("Removing the cache done.");
	}
}
