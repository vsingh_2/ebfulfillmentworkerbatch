package com.searshc.eb.tasklet;

import java.io.File;
import java.net.URI;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.searshc.eb.service.FulfillmentRuleService;

public class FileDownloadTasklet implements Tasklet, StepExecutionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileDownloadTasklet.class);

	// need to set these values in property file
	/*AWSCredentials credentials = new BasicAWSCredentials(Constants.USER_KEY, Constants.USER_SECRET);

	AmazonS3 s3client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
			.withRegion(Regions.US_EAST_1).build();*/
	
	@Autowired
	FulfillmentRuleService fulfillmentService;

	boolean stopJob;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
				String fileName = fulfillmentService.findFileToProcess();
				if (fileName != null) {
					URI fileUri = new URI(fileName);
					AmazonS3URI s3Uri = new AmazonS3URI(fileUri);
					AmazonS3 s3client = AmazonS3ClientBuilder.defaultClient();
					S3Object s3Object = s3client.getObject(s3Uri.getBucket(), s3Uri.getKey());
					LOGGER.info("Downloading file to process {} ", s3Uri.getKey());
					S3ObjectInputStream inputStream = s3Object.getObjectContent();
					FileUtils.copyInputStreamToFile(inputStream, new File("data/fulfillmentrule.xlsx"));
					stopJob = false;
				} else {
					LOGGER.info("No file left to process closing batch job");
					//contribution.setExitStatus(ExitStatus.FAILED);
					stopJob = true;
				}
		return RepeatStatus.FINISHED;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		if (!stopJob) {
			return ExitStatus.COMPLETED;
		} else if (stopJob) {
			return ExitStatus.NOOP;
		}
		return ExitStatus.FAILED;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.info("Starting step : " + stepExecution.getStepName());
	}
	
}
