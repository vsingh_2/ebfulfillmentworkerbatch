package com.searshc.eb.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.searshc.eb.processor.FulfillmentRuleProcessor;
import com.searshc.eb.service.FulfillmentRuleService;

public class UpdateUploadTasklet implements Tasklet {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpdateUploadTasklet.class);

	@Autowired
	FulfillmentRuleService fulfillmentService;

	@Autowired
	FulfillmentRuleProcessor ruleProcessor;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		boolean uploadFlag = true;
		int result = fulfillmentService.deleteRules(uploadFlag);
		if (result == 1) {
			LOGGER.info("work queue file status updated successfully");
		}
		LOGGER.info("Rules Deleted successfully with upload flag as true ");
		ruleProcessor.processFulfillmentRule();

		fulfillmentService.moveRules();
		String rulesOrderParam = fulfillmentService.reorderRules();
		if (rulesOrderParam != null) {
			if (!fulfillmentService.arrangeRulesByPriority(rulesOrderParam)) {
				System.out.println("error");
			}
		}
		return null;
	}
	
	

}
