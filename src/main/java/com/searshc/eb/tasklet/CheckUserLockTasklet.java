package com.searshc.eb.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.searshc.eb.service.FulfillmentRuleService;

public class CheckUserLockTasklet implements Tasklet, StepExecutionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckUserLockTasklet.class);

	@Autowired
	FulfillmentRuleService fulfillmentService;

	boolean stopJob;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		boolean  isLockAquired = fulfillmentService.checkFulfillmentLock();
		if (!isLockAquired) {
			fulfillmentService.aquireFulfillmentLock();
			stopJob = false;
		} else {
			stopJob = true;
		}
		LOGGER.info("Checking if user acquired the lock");
		return RepeatStatus.FINISHED;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		if (!stopJob) {
			return ExitStatus.COMPLETED;
		} else if (stopJob) {
			return ExitStatus.NOOP;
		}
		return ExitStatus.FAILED;
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.info("Starting step : " + stepExecution.getStepName());

	}

}
