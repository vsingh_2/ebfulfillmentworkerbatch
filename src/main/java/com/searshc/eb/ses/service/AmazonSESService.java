package com.searshc.eb.ses.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.SendTemplatedEmailRequest;
import com.amazonaws.services.simpleemail.model.SendTemplatedEmailResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.searshc.eb.constant.Constants;

@Component
public class AmazonSESService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AmazonSESService.class);
	private AmazonSimpleEmailService client;
	

	private void loadClient() throws Exception {
		 this.client = 
		          AmazonSimpleEmailServiceClientBuilder.standard()
		            .withRegion(Regions.US_EAST_1).build();
	}

	/*private void createTemplate() {
		Template template = new Template();
		template.setTemplateName(Constants.TEMPLATE_NAME);
		template.setSubjectPart(Constants.SUBJECT);
		template.setTextPart(Constants.EMAIL_TEXT);
		CreateTemplateRequest createTemplateRequest = new CreateTemplateRequest();
		createTemplateRequest.setTemplate(template);
		CreateTemplateResult result = client.createTemplate(createTemplateRequest);
		LOGGER.info("Created template response meta data - {}", result.getSdkResponseMetadata());
	}

	private void deleteTemplate() {
		DeleteTemplateRequest deleteTemplateRequest = new DeleteTemplateRequest();
		deleteTemplateRequest.setTemplateName(Constants.TEMPLATE_NAME);
		DeleteTemplateResult result = client.deleteTemplate(deleteTemplateRequest);
		LOGGER.info("Deleted template response meta data - {}", result.getSdkResponseMetadata());
	}*/

	public void sendEmail(EmailTemplateData emailTemplateData) throws Exception {
		loadClient();
		//createTemplate();
		try {
			Destination destination = new Destination();
			List<String> toAddresses = new ArrayList<String>();
			toAddresses.add(Constants.RECEVIER_EMAIL);
			destination.setToAddresses(toAddresses);
			SendTemplatedEmailRequest templatedEmailRequest = new SendTemplatedEmailRequest();
			templatedEmailRequest.withDestination(destination);
			templatedEmailRequest.withTemplate(Constants.TEMPLATE_NAME);
			templatedEmailRequest.withTemplateData(buildTemplateData(emailTemplateData));
			templatedEmailRequest.withSource(Constants.SENDER_EMAIL);
			LOGGER.info("Created tempalte email request - {}", templatedEmailRequest.getTemplate());
			SendTemplatedEmailResult templatedEmailResult = client.sendTemplatedEmail(templatedEmailRequest);
			System.out.println(templatedEmailResult.getMessageId());
			LOGGER.info("Sent message id - {}", templatedEmailResult.getMessageId());
		} catch(Exception ex){
			LOGGER.error("Exception occured while sending email - {}",ex.getMessage());
		}	/*finally {
			deleteTemplate();
		}*/
	}
	
	private String buildTemplateData(EmailTemplateData emailTemplateData) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(emailTemplateData);
	}


}
