package com.searshc.hs.batch.item.excel.support.rowset;

import com.searshc.hs.batch.item.excel.Sheet;

/**
 * {@code ColumnNameExtractor} which returns the values of a given row (default is 0) as the column
 * names.
 *
 */
public class RowNumberColumnNameExtractor implements ColumnNameExtractor {

    private int headerRowNumber;

    @Override
    public String[] getColumnNames(final Sheet sheet) {
        return sheet.getRow(headerRowNumber);
    }

    public void setHeaderRowNumber(int headerRowNumber) {
        this.headerRowNumber = headerRowNumber;
    }
}
