package com.searshc.hs.batch.item.excel.mapping;

import com.searshc.hs.batch.item.excel.RowMapper;
import com.searshc.hs.batch.item.excel.support.rowset.RowSet;

/**
 * Pass through {@link RowMapper} useful for passing the orginal String[]
 * back directly rather than a mapped object.
 *
 */
public class PassThroughRowMapper implements RowMapper<String[]> {

    @Override
    public String[] mapRow(final RowSet rs) throws Exception {
        return rs.getCurrentRow();
    }

}
