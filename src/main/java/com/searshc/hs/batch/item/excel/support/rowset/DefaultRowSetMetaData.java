package com.searshc.hs.batch.item.excel.support.rowset;

import com.searshc.hs.batch.item.excel.Sheet;

/**
 * Default implementation for the {@code RowSetMetaData} interface.
 *
 * Requires a {@code Sheet} and {@code ColumnNameExtractor} to operate correctly.
 * Delegates the retrieval of the column names to the {@code ColumnNameExtractor}.

 */
public class DefaultRowSetMetaData implements RowSetMetaData {

    private final Sheet sheet;

    private ColumnNameExtractor columnNameExtractor;

    DefaultRowSetMetaData(Sheet sheet) {
        this.sheet = sheet;
    }

    @Override
    public String[] getColumnNames() {
        return columnNameExtractor.getColumnNames(sheet);
    }

    @Override
    public String getColumnName(int idx) {
        String[] names = getColumnNames();
        return names[idx];
    }

    @Override
    public int getColumnCount() {
        return sheet.getNumberOfColumns();
    }

    @Override
    public String getSheetName() {
        return sheet.getName();
    }

    public void setColumnNameExtractor(ColumnNameExtractor columnNameExtractor) {
        this.columnNameExtractor = columnNameExtractor;
    }
}
