package com.searshc.hs.batch.item.excel;

import com.searshc.hs.batch.item.excel.support.rowset.RowSet;

/**
 * Callback to handle skipped lines. Useful for header/footer processing.

 */
public interface RowCallbackHandler {

    void handleRow(RowSet rs);

}
