package com.searshc.hs.batch.item.excel.support.rowset;

import com.searshc.hs.batch.item.excel.Sheet;

/**
 * {@code ColumnNameExtractor} implementation which returns a preset String[] to use as
 *  the column names. Useful for those situations in which an Excel file without a header row
 *  is read
 */
public class StaticColumnNameExtractor implements ColumnNameExtractor {

    private final String[] columnNames;

    public StaticColumnNameExtractor(String[] columnNames) {
        this.columnNames = columnNames;
    }

    @Override
    public String[] getColumnNames(Sheet sheet) {
        return this.columnNames;
    }

}
